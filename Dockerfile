FROM node:10.16 as build

WORKDIR /app
COPY package.json yarn.lock .eslintrc.json ./
COPY config ./config
COPY scripts ./scripts
COPY .storybook ./.storybook
COPY src ./src

RUN yarn install
RUN yarn storybook:build

FROM nginx:1.17-alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
RUN mkdir -p /var/public
COPY --from=build /app/.out /var/public
CMD cat /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'
EXPOSE 80
