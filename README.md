
# Use UIKit

Add dependency in your `package.json`:
```
"dependencies": {
    ...
    "uikit": "https://gitlab.com/sfinance-public/uikit.git#~v1.1.68",
    ...
}
```

Wrap your root app in `UIKitProvider` and include `css`:
```
import { UIKitProvider, Button } from 'uikit'
import 'uikit/lib/app.css'

const App = () => (
    <Button>Click!</Button>
)

const Root = () => (
    <UIKitProvider
        width={window.innerWidth}
        remPoint={14}
    >
        <App />
    </UIKitProvider>
)
```

More options for `<UIKitProvider>` (See `src/ui/Provider` for more details):
```
<UIKitProvider
    width={1024}
    responsiveModel={{
        isTouch: anyBoolean,
        isPhone: anyBoolean,
        isTablet: anyBoolean,
        isTabletAndPhone: anyBoolean,
        isDesktop: anyBoolean
    }}
    remPoint={14}
    label={{ weight: 600 }}
>
    <App />
</UIKitProvider>
```

Use `import { colors, breakPoints, viewPorts } from 'uikit'` for theme of `styled-components` in your project:
```
import { colors, breakPoints, viewPorts } from 'uikit'
import { ThemeProvider } from 'styled-components'

<ThemeProvider theme={{

```

# Development

**Requirements**:

- [Node.js >= 10](https://nodejs.org/en/)
- [Yarn package manager](https://yarnpkg.com)
- `yarn install`

First, See `src/index.js` for list of export components & utils.

Next, start `Storybook`

**Run shell:**

```
$ yarn storybook
```

and see [http://localhost:9001](http://localhost:9001)

# Prepare for publish

- Export your component from `ui/...` or your helper `utils/...` in `src/index.js`
- Build minify `js`:
    ```
    $ yarn build
    ```
- Change version in `package.json:version`
- Create version `git tag`
