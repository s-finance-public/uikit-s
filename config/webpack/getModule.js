const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const config = require('../config')

const NODE_ENV = config.NODE_ENV

module.exports = () => ({
  rules: [
    {
      enforce: 'pre',
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'eslint-loader'
    },
    {
      test: /\.(js|jsx)$/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react'
            ],
            plugins: [
              ['@babel/plugin-transform-runtime'],
              ['@babel/plugin-proposal-decorators', { legacy: true }],
              ['@babel/plugin-proposal-class-properties', { loose: true }],
              '@babel/plugin-proposal-export-default-from',
              'react-hot-loader/babel'
            ]
          }
        }
      ],
      exclude: /node_modules/
    },
    {
      test: /\.(less|css)$/,
      use: NODE_ENV === 'production' ? [
        MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader'
        },
        {
          loader: 'less-loader',
          options: {
            javascriptEnabled: true
          }
        }
      ] : [
        'style-loader',
        'css-loader',
        {
          loader: 'less-loader',
          options: {
            javascriptEnabled: true
          }
        }
      ]
    },
    {
      test: /\.((woff(2)?)|ttf|eot|otf)(\?[\d#&.=a-z]+)?$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            name: 'fonts/[name].[ext]'
          }
        }
      ]
    },
    {
      test: /\.hbs$/,
      use: [
        {
          loader: 'handlebars-loader'
        }
      ]
    },
    {
      test: /\.(png|jpg|gif|svg|doc|docx)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: 'resources/[name].[ext]'
          }
        }
      ]
    }
  ]
})
