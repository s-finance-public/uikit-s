const path = require('path')
const constants = require('./constants')

module.exports = () => [
  path.resolve(constants.SRC_PATH, 'index.js')
]
