const nconf = require('nconf')

nconf.formats.yaml = require('nconf-yaml')

const cfg = nconf.argv()
cfg.env()

module.exports = {
  NODE_ENV: nconf.get('NODE_ENV')
}
