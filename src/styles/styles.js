import './antd/index.less'

const grayColors = {
  gray1: '#f4f5f6',
  gray2: '#e8ebef',
  gray3: '#c4ccd4',
  gray4: '#7a818a',
  gray5: '#4a5159',
  gray6: '#363f4d',
  gray7: '#262f40',
  gray8: '#171f33',
  gray9: '#0d152b'
}

const blueColors = {
  blue1: '#eaf7fe',
  blue2: '#c6e9fb',
  blue3: '#8dd4f7',
  blue4: '#6bc5f2',
  blue5: '#3eb5f1',
  blue6: '#2198e8',
  blue7: '#1f6bad',
  blue8: '#134c8b',
  blue9: '#032e6d'
}

const greenColors = {
  green1: '#e7fbd5',
  green2: '#ccf8ac',
  green3: '#a5ea7d',
  green4: '#7cd459',
  green5: '#49b82b',
  green6: '#319e1e',
  green7: '#1e8416',
  green8: '#0d6a0e',
  green9: '#07570d'
}

const redColors = {
  red1: '#fdded9',
  red2: '#fdbbb1',
  red3: '#fd9a8b',
  red4: '#fd806d',
  red5: '#fd573e',
  red6: '#d9372c',
  red7: '#b51f22',
  red8: '#921421',
  red9: '#780b1f'
}

const goldColors = {
  gold1: '#fcf6d1',
  gold2: '#faeca3',
  gold3: '#f6da74',
  gold4: '#edc950',
  gold5: '#e3ae1a',
  gold6: '#c28f13',
  gold7: '#a2720d',
  gold8: '#825706',
  gold9: '#6b4605'
}

const baseColors = {
  ...grayColors,

  ...blueColors,

  primary1: greenColors.green1,
  primary2: greenColors.green2,
  primary3: greenColors.green3,
  primary4: greenColors.green4,
  primary5: greenColors.green5,
  primary6: greenColors.green6,
  primary7: greenColors.green7,
  primary8: greenColors.green8,
  primary9: greenColors.green9,

  ...greenColors,

  ...goldColors,

  ...redColors,

  lightBlue: blueColors.blue2,
  darkSkyBlue: blueColors.blue5,
  darkBlue: blueColors.blue7,

  successLight: greenColors.green4,
  successBase: greenColors.green5,
  successDark: greenColors.green8,

  errorLight: redColors.red4,
  errorBase: redColors.red5
}

export const colors = {
  white: grayColors.gray1,
  black: grayColors.gray9,
  text: grayColors.gray9,

  default: grayColors.gray4,
  secondary: grayColors.gray4,
  disable: '#bac4cd',

  primary: baseColors.primary5,
  info: baseColors.green4,
  success: baseColors.green5,
  danger: baseColors.red5,
  warning: baseColors.gold5,

  background: '#f4f7ff',

  ...baseColors
}

export const breakPoints = {
  mobile: 768,
  tablet: 1200,
  desktop: 1400,
  largeDesktop: 1600
}

export const viewPortsResolve = points => ({
  mobile: `@media screen and (max-width: ${points.mobile - 1}px)`,
  tablet: `@media screen and (min-width: ${points.mobile}px) and (max-width: ${points.tablet - 1}px)`,
  touch: `@media screen and (max-width: ${points.tablet - 1}px)`,
  beforeDesktop: `@media screen and (max-width: ${points.desktop - 1}px)`,
  desktop: `@media screen and (min-width: ${points.desktop}px)`,
  largeDesktop: `@media screen and (min-width: ${points.largeDesktop}px)`
})

export const theme = {
  colors,
  breakPoints
}
