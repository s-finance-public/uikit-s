import React from 'react'
import { storiesOf } from '@storybook/react'
import Tooltip from './Tooltip'

const stories = storiesOf('Tooltip', module)

stories.addDecorator(story => (
    <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', height: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
    <Tooltip tooltip="Tooltip">
      <div>Default bottom tooltip</div>
    </Tooltip>
  ))
  .add('left tooltip', () => (
    <Tooltip tooltip="Tooltip" placement="left">
      <div>Left tooltip</div>
    </Tooltip>
  ))
  .add('right tooltip', () => (
    <Tooltip tooltip="Tooltip" placement="right">
        <div>Right tooltip</div>
    </Tooltip>
  ))
  .add('top tooltip', () => (
    <Tooltip tooltip="Tooltip" placement="top">
        <div>Top tooltip</div>
    </Tooltip>
  ))
  .add('force visible', () => (
    <Tooltip tooltip="Tooltip" placement="top" isVisible>
        <div>Top tooltip</div>
    </Tooltip>
  ))
