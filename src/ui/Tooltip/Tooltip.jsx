import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import AntdTooltip from 'antd/es/tooltip'
import { isSafe, safeObject } from 'utils/object'
import { Context } from '../Provider/Context'

const Tooltip = ({ children, placement, tooltip, isInline, isVisible, onVisibleChange, overlayStyle }) => {
  const { responsiveModel: { isTouch } } = useContext(Context)

  if (isTouch) return children

  return (
    tooltip ?
      <AntdTooltip
          placement={placement}
          title={tooltip}
          overlayStyle={overlayStyle}
          {...safeObject({
            visible: isSafe(isVisible) ? isVisible : null,
            onVisibleChange
          })}
      >
        {isInline ?
            <span>{children}</span> :
            <div>{children}</div>
        }
      </AntdTooltip> : children
  )
}

Tooltip.propTypes = {
  placement: PropTypes.oneOf([
    'top', 'left', 'right', 'bottom',
    'topLeft', 'topRight',
    'bottomLeft', 'bottomRight',
    'leftTop', 'leftBottom',
    'rightTop', 'rightBottom'
  ]),
  tooltip: PropTypes.any,
  isInline: PropTypes.bool,
  isVisible: PropTypes.any,
  onVisibleChange: PropTypes.func,
  overlayStyle: PropTypes.object
}

Tooltip.defaultProps = {
  placement: 'bottom',
  tooltip: null,
  isInline: false,
  isVisible: null,
  onVisibleChange: null,
  overlayStyle: {}
}

Tooltip.displayName = 'Tooltip'

export default Tooltip
