import React, { Fragment, useState } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Button from 'ui/Button'
import { Paragraph, Text } from 'ui/Typography'
import { Row } from 'ui/Grid'
import Modal from './Modal'

const stories = storiesOf('Modal', module)

const ButtonHelper = ({ title, Component, ...props }) => {
  const [isOpen, setIsOpen] = useState(false)

  const handleOpen = () => {
    setIsOpen(true)
  }

  const handleOk = () => {
    setIsOpen(false)
    action('onOk')
  }

  const handleCancel = () => {
    setIsOpen(false)
    action('onCancel')
  }

  return (
    <Fragment>
        <Button type="primary" onClick={handleOpen}>
            {title}
        </Button>
        <Component isOpen={isOpen} onOk={handleOk} onCancel={handleCancel}/>
    </Fragment>
  )
}

const ExampleContainer = () => (
    <Fragment>
        <Paragraph>One</Paragraph>
        <Paragraph>Two</Paragraph>
        <Paragraph>Three</Paragraph>
        <Paragraph>Four</Paragraph>
    </Fragment>
)

stories
  .add('default', () => (
    <ButtonHelper
        title="Open default modal"
        Component={({ isOpen, onOk, onCancel }) => (
            <Modal isOpen={isOpen} okOk={onOk} onCancel={onCancel} title="Default modal">
                <ExampleContainer/>
            </Modal>
        )}
    />
  ))
  .add('inline closable', () => (
    <ButtonHelper
        title="Open inline closable modal"
        Component={({ isOpen, onOk, onCancel }) => (
            <Modal isOpen={isOpen} okOk={onOk} onCancel={onCancel} title="Default modal" isInlineClosable>
                <ExampleContainer/>
            </Modal>
        )}
    />
  ))
  .add('without closable', () => (
    <ButtonHelper
        title="Open modal without closable"
        Component={({ isOpen, onOk, onCancel }) => (
            <Modal isOpen={isOpen} okOk={onOk} onCancel={onCancel} title="Without closable" isClosable={false}>
                <ExampleContainer/>
            </Modal>
        )}
    />
  ))
  .add('centered', () => (
    <ButtonHelper
        title="Open centered modal"
        Component={({ isOpen, onOk, onCancel }) => (
            <Modal isOpen={isOpen} okOk={onOk} onCancel={onCancel} title="Centered" isCenter>
                <ExampleContainer/>
            </Modal>
        )}
    />
  ))
  .add('custom width && height', () => (
    <ButtonHelper
        title="Open modal with custom width && height"
        Component={({ isOpen, onOk, onCancel }) => (
            <Modal isOpen={isOpen} okOk={onOk} onCancel={onCancel} title="Custom width" width="80%" height="80rem">
                <ExampleContainer/>
            </Modal>
        )}
    />
  ))
  .add('with footer', () => (
    <ButtonHelper
        title="Open with footer"
        Component={({ isOpen, onCancel }) => (
            <Modal
                isOpen={isOpen}
                onCancel={onCancel}
                title="With footer"
                footer={
                  <Row isFlex justify="center">
                    <Button type="primary">Click</Button>
                  </Row>
                }
            >
                <ExampleContainer/>
            </Modal>
        )}
    />
  ))

stories
  .add('compositions', () => (
    <ButtonHelper
      title="click"
      Component={({ isOpen, onCancel }) => (
        <Modal
          isOpen={isOpen}
          onCancel={onCancel}
          isCenter
          title="СМС информирование"
          footer={
            <Row isFlex justify="center" isMarginBottom>
              <Button type="primary">Выбрать услугу</Button>
            </Row>
          }
        >
          <Paragraph>
            СМС-информирование – это автоматизированная услуга, позволяющая клиенту получать уведомления о приближении даты уплаты ежемесячного платежа по кредиту. Данная услуга позволит не только не пропустить очередной платеж по кредиту, но и сохранить положительной Вашу кредитную историю.
          </Paragraph>
          <Text isStrong>
            Покрытие с перечислением
          </Text>
          <Paragraph isMarginTop marginTop={0.5}>
            <ul>
              <li>Периодичность Уведомлений соответствует периодичности платежей, установленной Графиком платежей Кредитного договора</li>
              <li>Уведомления осуществляются Обществом за 3 (три) календарных дня до наступления каждой даты уплаты ежемесячного платежа, зафиксированной Графиком платежей Кредитного договора.</li>
            </ul>
          </Paragraph>
          <Text isStrong>Стоимость сервиса</Text>
          <Paragraph isMarginTop marginTop={0.5}>50 рублей в месяц</Paragraph>
          <Text isStrong>Контакты</Text>
          <Paragraph isMarginTop marginTop={0.5}>
            Общество с ограниченной ответственностью «Финсервис», адрес (место нахождения): 117105, г. Москва, Варшавское ш., д. 1, стр. 1 – 2, комн. 38, оф. В412
          </Paragraph>
        </Modal>
      )}
    />
  ))
