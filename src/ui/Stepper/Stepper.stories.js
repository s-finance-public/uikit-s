import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Stepper from './Stepper'

const stories = storiesOf('Stepper', module)

stories.addDecorator(story => (
    <div style={{ width: '70%' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
        <Stepper
            options={[
              { title: 'One' },
              { title: 'Two' },
              { title: 'Three' },
              { title: 'Four' }
            ]}
            onChange={action('changed')}
        />
  ))
  .add('with description', () => (
    <Stepper
        options={[
          { title: 'One', description: 'One description' },
          { title: 'Two', description: 'Two description' },
          { title: 'Three', description: 'Three description' },
          { title: 'Four', description: 'Four description' }
        ]}
        onChange={action('changed')}
    />
  ))
  .add('with any status', () => (
    <Stepper
        current={3}
        options={[
          { title: 'One', description: 'One description', status: 'success' },
          { title: 'Two', description: 'Two description', status: 'wait' },
          { title: 'Three', description: 'Three description', status: 'process' },
          { title: 'Four', description: 'Four description', status: 'error' },
          { title: 'Six', description: 'Four description', status: 'finish' }
        ]}
        onChange={action('changed')}
    />
  ))
  .add('vertical placement', () => (
    <Stepper
        current={3}
        placement="vertical"
        options={[
          { title: 'One', description: 'One description', status: 'success' },
          { title: 'Two', description: 'Two description', status: 'wait' },
          { title: 'Three', description: 'Three description', status: 'process' },
          { title: 'Four', description: 'Four description', status: 'error' },
          { title: 'Six', description: 'Four description', status: 'finish' }
        ]}
        onChange={action('changed')}
    />
  ))
