import Uce244278a1d34fbca17611768ee5c83e from './banking'
import U158bbee4f0d24b349953288467dac2e4 from './calc'
import U93cc32a348db406bb2e7c53482cacdbd from './calendar'
import U4845cc29b39d48c5a3fcab6f240a2f81 from './camera'
import U20a4c7953d364351acd83488c7bcd232 from './caret-down'
import Uf56751d846ef4359847ce610e698a03e from './chevron-down'
import U4a7224781b3f41b08f8f6daa09e4a785 from './cog'
import U77cf49c139b24b58abfd8752dbbf32f5 from './cogs'
import U532b176a23c74e2ba027d0c1372aa69c from './down'
import Uca95f744476240f89acfde9dd5548a8c from './duplicate'
import U0afaa1ec889a47bc94fdc7a4c0e40321 from './edit'
import U7f02a6a8b9df4efda637e71200570ff9 from './expand'
import U32409bf8135b4eef9bc0d2f299fb1109 from './eye'
import Ud1365fb5f1e64bab86089c4f7ca1b2ff from './globe'
import U42edb0c8143e445db674030199e47521 from './globe2'
import Uba546072eb3a4540aca87b5a4fe0f1eb from './home'
import U11fed6d9678546e386c3586e1f33a7f3 from './level-down'
import U5e60de1e18c842d58732849ed6cba03a from './logo-marker'
import U7974a2b25c4a459186e86fba8e817562 from './map'
import Ubd9da5d2c2964279bd02b375920f5243 from './menu'
import Ue21aa5d352a54df8886014ae8550c68c from './message-edit'
import Ue34563ef1d394ea49cbd808883d3777d from './message'
import Uee798bdafc7948cfbee5fa0c93d57db2 from './message2'
import U81f5fbe281444a47806f4a9e3510690b from './offer-details'
import Uef9c4d5fe527413591e9df856d962de2 from './old-phone'
import U1bd858d92aaf40a1bdcc656378a687d3 from './phone'
import Uff9c1ca1a6244d90a7335aea70717e64 from './phone2'
import Uc9d1e8a03f7e428da97589f9059cc60b from './question'
import U8c78c6bb45e84f95ac4efc0e0abd8fdc from './search'
import Uc49fa72b36714e968df37ddc38d06908 from './sms'
import Ua486dd5a24c7424e93983386df50abd4 from './sort-up'
import U55c0c2f304664ef398336d671706d13a from './sort'
import Ue2e7894a91a04619bb83e5896e800649 from './trash'
import U286dbd5555b34602aa83462fd751f3ea from './user'
import U52560af8484e4bb28b26111a57802aa9 from './user2'
import U7b161dc0b9144614a0fcfbb3a286a12c from './wrench'
import U79adbc5e39084922b063a1a148624523 from './zoom-in'

export default {
  banking: Uce244278a1d34fbca17611768ee5c83e,
  calc: U158bbee4f0d24b349953288467dac2e4,
  calendar: U93cc32a348db406bb2e7c53482cacdbd,
  camera: U4845cc29b39d48c5a3fcab6f240a2f81,
  'caret-down': U20a4c7953d364351acd83488c7bcd232,
  'chevron-down': Uf56751d846ef4359847ce610e698a03e,
  cog: U4a7224781b3f41b08f8f6daa09e4a785,
  cogs: U77cf49c139b24b58abfd8752dbbf32f5,
  down: U532b176a23c74e2ba027d0c1372aa69c,
  duplicate: Uca95f744476240f89acfde9dd5548a8c,
  edit: U0afaa1ec889a47bc94fdc7a4c0e40321,
  expand: U7f02a6a8b9df4efda637e71200570ff9,
  eye: U32409bf8135b4eef9bc0d2f299fb1109,
  globe: Ud1365fb5f1e64bab86089c4f7ca1b2ff,
  globe2: U42edb0c8143e445db674030199e47521,
  home: Uba546072eb3a4540aca87b5a4fe0f1eb,
  'level-down': U11fed6d9678546e386c3586e1f33a7f3,
  'logo-marker': U5e60de1e18c842d58732849ed6cba03a,
  map: U7974a2b25c4a459186e86fba8e817562,
  menu: Ubd9da5d2c2964279bd02b375920f5243,
  'message-edit': Ue21aa5d352a54df8886014ae8550c68c,
  message: Ue34563ef1d394ea49cbd808883d3777d,
  message2: Uee798bdafc7948cfbee5fa0c93d57db2,
  'offer-details': U81f5fbe281444a47806f4a9e3510690b,
  'old-phone': Uef9c4d5fe527413591e9df856d962de2,
  phone: U1bd858d92aaf40a1bdcc656378a687d3,
  phone2: Uff9c1ca1a6244d90a7335aea70717e64,
  question: Uc9d1e8a03f7e428da97589f9059cc60b,
  search: U8c78c6bb45e84f95ac4efc0e0abd8fdc,
  sms: Uc49fa72b36714e968df37ddc38d06908,
  'sort-up': Ua486dd5a24c7424e93983386df50abd4,
  sort: U55c0c2f304664ef398336d671706d13a,
  trash: Ue2e7894a91a04619bb83e5896e800649,
  user: U286dbd5555b34602aa83462fd751f3ea,
  user2: U52560af8484e4bb28b26111a57802aa9,
  wrench: U7b161dc0b9144614a0fcfbb3a286a12c,
  'zoom-in': U79adbc5e39084922b063a1a148624523
}
