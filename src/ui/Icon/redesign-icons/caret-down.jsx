import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 17" {...props}><path fill="currentColor" fillRule="evenodd" d="M11.77 7.955l-3.122 3.107c-.365.363-.954.363-1.319 0L4.207 7.955c-.202-.2-.262-.47-.154-.734.109-.263.365-.402.65-.402h6.571c.285 0 .541.139.65.402.109.263.048.533-.154.734z" /></svg>

export default Icon
