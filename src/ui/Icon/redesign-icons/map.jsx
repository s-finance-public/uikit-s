import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 16" {...props}><path fill="currentColor" fillRule="evenodd" d="M8 0C4.691 0 2 2.692 2 6c0 4.426 5.536 9.688 5.771 9.91.065.06.147.09.229.09.082 0 .164-.03.229-.09C8.464 15.686 14 10.425 14 6c0-3.308-2.691-6-6-6zm0 9.333C6.162 9.333 4.667 7.838 4.667 6S6.162 2.667 8 2.667 11.333 4.162 11.333 6 9.838 9.333 8 9.333z" /></svg>

export default Icon
