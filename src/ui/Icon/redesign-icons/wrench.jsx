import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 100.000000 100.000000" preserveAspectRatio="xMidYMid meet" {...props}><metadata>{'\nCreated by potrace 1.16, written by Peter Selinger 2001-2019\n'}</metadata><g transform="translate(0.000000,100.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M532 863 c-57 -20 -126 -91 -146 -150 -9 -25 -16 -70 -16 -99 l0 -52 -129 -122 c-71 -67 -137 -137 -145 -155 -58 -122 52 -243 178 -195 14 5 87 70 164 145 137 133 140 135 184 135 175 0 287 149 248 331 l-13 60 -66 -65 c-63 -63 -69 -66 -114 -66 l-47 0 0 47 c0 45 3 51 66 114 l66 67 -51 11 c-69 15 -127 13 -179 -6z" /></g></svg>

export default Icon
