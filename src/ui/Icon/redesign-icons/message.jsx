import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 12 12" {...props}><path fill="currentColor" fillRule="evenodd" d="M6 1.5c3.314 0 6 2.015 6 4.5s-2.686 4.5-6 4.5c-.469 0-.925-.04-1.363-.117L1.53 11.766c-.189.084-.41-.001-.495-.19-.033-.074-.04-.157-.023-.236l.532-2.327C.584 8.216 0 7.16 0 6c0-2.485 2.686-4.5 6-4.5z" /></svg>

export default Icon
