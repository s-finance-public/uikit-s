import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 16" {...props}><path fill="currentColor" fillRule="evenodd" d="M4 6v4c0 1.054.816 1.918 1.85 1.995L6 12h4c0 1.105-.895 2-2 2H4c-1.105 0-2-.895-2-2V8c0-1.105.895-2 2-2zm8-4c1.105 0 2 .895 2 2v4c0 1.105-.895 2-2 2H8c-1.105 0-2-.895-2-2V4c0-1.105.895-2 2-2h4z" /></svg>

export default Icon
