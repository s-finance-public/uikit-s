import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 16" {...props}><path fill="currentColor" fillRule="evenodd" d="M8 2c3.697 0 6.833 2.507 8 6-1.167 3.493-4.303 6-8 6s-6.833-2.507-8-6c1.167-3.493 4.303-6 8-6zm0 2C5.79 4 4 5.79 4 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm1.414 2.586c.781.78.781 2.047 0 2.828-.78.781-2.047.781-2.828 0-.781-.78-.781-2.047 0-2.828.78-.781 2.047-.781 2.828 0z" /></svg>

export default Icon
