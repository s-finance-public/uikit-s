import _ from 'lodash'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import AntdIcon from 'antd/es/icon'
import withMargin from 'ui/common/withMargin'
import { colors } from 'styles'
import { Context } from 'ui/Provider/Context'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'
import designIcons from './design-icons'
import redesignIcons from './redesign-icons'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'color')
  _.unset(resolveProps, 'isPointer')
  _.unset(resolveProps, 'iconType')
  return <AntdIcon {...resolveProps}/>
})`
  cursor: ${props => (props.isPointer ? 'pointer' : 'inherit')};
  transition: color .3s ease-in-out;
`

const Icon = ({
  className, style, htmlID, dataSet,

  icon, iconType, onClick, size, color, isFill, twoToneColor, isRem, isSpin, rotate
}) => {
  const { remPoint } = useContext(Context)

  let resolveProps = {
    className,
    onClick,
    isPointer: !!onClick,
    style: safeObject({
      color: colors[color] || color,
      fontSize: size ? `${isRem ? size * remPoint : size}px` : null,
      ...style
    }),
    iconType,
    ...safeObject({
      theme: isFill ? 'filled' : !twoToneColor ? 'outlined' : 'twoTone',
      twoToneColor: colors[twoToneColor] ? colors[twoToneColor] : twoToneColor,
      id: htmlID || null
    }),
    spin: isSpin,
    rotate,
    ...applyHtmlData(dataSet)
  }
  switch (iconType) {
    case 'ant': {
      resolveProps = {
        ...resolveProps,
        type: icon
      }
      break
    }
    case 'design': {
      resolveProps = {
        ...resolveProps,
        component: designIcons[icon]
      }
      break
    }
    case 'redesign': {
      resolveProps = {
        ...resolveProps,
        component: redesignIcons[icon]
      }
      break
    }
  }

  return (
    <Root {...resolveProps}/>
  )
}

Icon.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  icon: PropTypes.any,
  color: PropTypes.string, // colors of theme or custom color
  isFill: PropTypes.bool,
  iconType: PropTypes.oneOf(['ant', 'design', 'redesign']),
  onClick: PropTypes.func,
  rotate: PropTypes.number,
  size: PropTypes.number,
  isRem: PropTypes.bool,
  isSpin: PropTypes.bool,
  twoToneColor: PropTypes.string
}

Icon.defaultProps = {
  icon: '',
  color: 'inherit',
  isFill: null,
  iconType: 'ant',
  size: 0,
  isRem: true,
  isSpin: false,
  twoToneColor: null,
  rotate: 0
}

Icon.displayName = 'Icon'

export default withMargin({ displayName: 'Icon' })(Icon)
