import fp from 'lodash/fp'
import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Icon from './Icon'

const stories = storiesOf('Icon', module)

fp.forEach(color => {
  stories.add(`color: ${color}`, () => (
    <Fragment>
        <Icon color={color} icon="eye" size={5}/>
        <Icon color={color} iconType="redesign" icon="globe2" size={5}/>
        <Icon color={color} iconType="redesign" icon="sms" size={5}/>
        <Icon color={color} iconType="redesign" icon="message-edit" size={5}/>
        <Icon color={color} iconType="redesign" icon="message2" size={5}/>
    </Fragment>
  ))
})(['primary', 'info', 'success', 'danger', 'warning'])

stories
  .add('size: 5rem', () => (
    <Icon icon="eye" size={5}/>
  ))
  .add('rotate', () => (
    <Icon icon="eye" size={5} rotate={45}/>
  ))
  .add('design icon', () => (
    <Fragment>
      <Icon size={5} iconType="design" icon="question" isMarginRight/>
      <Icon size={5} iconType="design" icon="close" isMarginRight/>
      <Icon size={5} iconType="design" icon="check" isMarginRight/>
    </Fragment>
  ))
  .add('spin', () => (
    <Icon icon="pie-chart" size={5} isSpin/>
  ))
  .add('with onClick', () => (
      <Icon icon="edit" size={5} onClick={action('click icon')}/>
  ))
  .add('fill icon', () => (
    <Fragment>
      <Icon icon="eye" size={5} isFill isMarginRight/>
      <Icon icon="eye" size={5} isFill color="primary"/>
    </Fragment>
  ))
  .add('two tone color icon', () => (
    <Fragment>
      <Icon icon="eye" size={5} twoToneColor="warning" isMarginRight/>
      <Icon icon="eye" size={5} twoToneColor="primary"/>
    </Fragment>
  ))
  .add('two designs', () => (
    <Fragment>
      <Icon size={5} iconType="design" icon="question" isMarginRight/>
      <Icon size={5} iconType="redesign" icon="question" isMarginRight/>
    </Fragment>
  ))
