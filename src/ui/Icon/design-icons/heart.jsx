import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 15" {...props}><path fillRule="nonzero" d="M8.004 15c-.308 0-.664-.145-.997-.362C2.705 11.82 0 8.438 0 5.097 0 2.06 1.993 0 4.58 0 5.97 0 7.15.668 8.003 2.126 8.858.676 10.029 0 11.421 0 14.007 0 16 2.061 16 5.097c0 3.341-2.697 6.723-7.007 9.54-.333.218-.689.363-.989.363z" /></svg>

export default Icon
