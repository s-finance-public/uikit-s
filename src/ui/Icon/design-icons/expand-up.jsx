import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 9" {...props}><path fill="currentColor" fillRule="nonzero" d="M8 0c-.316 0-.577.123-.83.37L.316 7.22A1 1 0 0 0 0 7.952C0 8.533.469 9 1.073 9c.298 0 .56-.123.767-.317L8.009 2.51l6.151 6.173c.217.203.487.317.776.317A1.05 1.05 0 0 0 16 7.952c0-.29-.1-.537-.307-.73L8.84.37A1.15 1.15 0 0 0 8 0z" /></svg>

export default Icon
