import React from 'react'
import { storiesOf } from '@storybook/react'
import Button from 'ui/Button'
import { Paragraph, Text } from 'ui/Typography'
import Popover from './Popover'

const stories = storiesOf('Popover', module)

stories.addDecorator(story => (
    <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', height: '40rem' }}>
        {story()}
    </div>
))

const content = (
    <div>
        <Paragraph><Text isStrong>One</Text> content</Paragraph>
        <Paragraph><Text isStrong>Two</Text> content</Paragraph>
        <Paragraph><Text isStrong>Three</Text> content</Paragraph>
        <Paragraph><Text isStrong>Four</Text> content</Paragraph>
    </div>
)

stories
  .add('default', () => (
    <Popover content={content}>
        <Button>
            Default bottom popover
        </Button>
    </Popover>
  ))
  .add('left popover', () => (
    <Popover content={content} placement="left">
        <Button>
            Left popover
        </Button>
    </Popover>
  ))
  .add('right popover', () => (
    <Popover content={content} placement="right">
        <Button>
            Right popover
        </Button>
    </Popover>
  ))
  .add('top popover', () => (
    <Popover content={content} placement="top">
        <Button>
            Top popover
        </Button>
    </Popover>
  ))
  .add('controlled', () => (
    <Popover content={content} isVisible>
        <Button>
            Top popover
        </Button>
    </Popover>
  ))
