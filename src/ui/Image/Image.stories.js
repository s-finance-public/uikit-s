import React from 'react'
import { storiesOf } from '@storybook/react'
import imgExample from './bankLogo1.png'
import Image from './Image'

const stories = storiesOf('Image', module)

stories
  .add('default', () => (
    <Image src={imgExample}/>
  ))
  .add('with width & height', () => (
    <Image src={imgExample} width="20rem" height="20rem"/>
  ))
  .add('avatar', () => (
    <Image src={imgExample} width="20rem" height="20rem" isAvatar/>
  ))
