import React, { Fragment, useState } from 'react'
import { storiesOf } from '@storybook/react'
import Button from 'ui/Button'
import { Paragraph } from 'ui/Typography'
import Drawer from './Drawer'

const stories = storiesOf('Drawer', module)

const ButtonHelper = ({ title, Component, ...props }) => {
  const [isOpen, setIsOpen] = useState(false)

  const handleOpen = () => {
    setIsOpen(true)
  }

  const handleClose = () => {
    setIsOpen(false)
  }

  return (
    <Fragment>
        <Button type="primary" onClick={handleOpen}>
            {title}
        </Button>
        <Component isOpen={isOpen} onClose={handleClose}/>
    </Fragment>
  )
}

const ExampleContainer = () => (
    <Fragment>

        <Paragraph>One</Paragraph>
        <Paragraph>Two</Paragraph>
        <Paragraph>Three</Paragraph>
        <Paragraph>Four</Paragraph>
    </Fragment>
)

stories
  .add('default', () => (
    <ButtonHelper
        title="Open default drawer"
        Component={({ isOpen, onClose }) => (
            <Drawer isOpen={isOpen} onClose={onClose} title="Default drawer">
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
  .add('with custom width', () => (
    <ButtonHelper
        title="Open drawer with custom width"
        Component={({ isOpen, onClose }) => (
            <Drawer isOpen={isOpen} onClose={onClose} title="Custom width" width="50%">
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
  .add('with right placement', () => (
    <ButtonHelper
        title="Open right drawer"
        Component={({ isOpen, onClose }) => (
            <Drawer isOpen={isOpen} onClose={onClose} title="Right placement" width="50%" placement="right">
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
  .add('with bottom placement', () => (
    <ButtonHelper
        title="Open bottom drawer"
        Component={({ isOpen, onClose }) => (
            <Drawer isOpen={isOpen} onClose={onClose} title="Bottom placement" placement="bottom">
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
  .add('with top placement', () => (
    <ButtonHelper
        title="Open top drawer"
        Component={({ isOpen, onClose }) => (
            <Drawer isOpen={isOpen} onClose={onClose} title="Top placement" placement="top">
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
  .add('with top placement with & height', () => (
    <ButtonHelper
        title="Open top drawer with & height"
        Component={({ isOpen, onClose }) => (
            <Drawer isOpen={isOpen} onClose={onClose} title="Top placement with height" placement="top" height="100%    ">
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
  .add('with custom body style', () => (
    <ButtonHelper
        title="Open drawer with custom body style"
        Component={({ isOpen, onClose }) => (
            <Drawer isOpen={isOpen} onClose={onClose} title="Custom body style" bodyStyle={{ background: '#eee' }}>
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
  .add('without closable', () => (
    <ButtonHelper
        title="Open drawer without closable"
        Component={({ isOpen, onClose }) => (
            <Drawer isOpen={isOpen} onClose={onClose} title="Without closable" isClosable={false}>
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
  .add('with footer', () => (
    <ButtonHelper
        title="Open drawer with footer"
        Component={({ isOpen, onClose }) => (
            <Drawer
                isOpen={isOpen}
                onClose={onClose}
                title="With footer"
                width="50%"
                placement="right"
                footer={<div>!!!</div>}
            >
                <ExampleContainer/>
                <ExampleContainer/>
                <ExampleContainer/>
                <ExampleContainer/>
                <ExampleContainer/>
                <ExampleContainer/>
            </Drawer>
        )}
    />
  ))
