import { createContext } from 'react'

const Context = createContext({})

const ContextProvider = Context.Provider
const ContextConsumer = Context.Consumer

export { ContextProvider, ContextConsumer, Context }
