import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import ruRu from 'antd/lib/locale-provider/ru_RU'
import ConfigProvider from 'antd/es/config-provider'
import 'moment/locale/es-us'
import { breakPoints, colors } from 'styles'
import 'utils/moment'
import { createGlobalStyle } from 'styled-components'
import { isSafe } from 'utils/object'
import { ContextProvider } from './Context'


const GlobalStyles = createGlobalStyle`
  .ant-typography a {
    color: inherit !important;
  }
  
  .ant-typography a:hover {
    color: ${colors.primary} !important;
  }
  
  a:hover {
    color: ${colors.primary} !important;
  }
`

const Provider = ({
  width,
  remPoint = 14,
  label = {
    weight: 600
  },
  children,
  responsiveModel,
  Link
}) => {
  const isTouch = responsiveModel?.isTouch ?? null
  const isMobile = _.get(responsiveModel, 'isMobile', null)
  const isTablet = _.get(responsiveModel, 'isTablet', null)
  const isTabletAndPhone = _.get(responsiveModel, 'isTabletAndPhone', null)
  const isBeforeDesktop = _.get(responsiveModel, 'isBeforeDesktop', null)
  const isDesktop = _.get(responsiveModel, 'isDesktop', null)
  const isMiddleDesktop = _.get(responsiveModel, 'isMiddleDesktop', null)
  const isLargeDesktop = _.get(responsiveModel, 'isLargeDesktop', null)

  const mobile = responsiveModel?.breakPoints?.mobile ?? breakPoints.mobile
  const tablet = responsiveModel?.breakPoints?.tablet ?? breakPoints.tablet
  const desktop = responsiveModel?.breakPoints?.desktop ?? breakPoints.desktop
  const largeDesktop = responsiveModel?.breakPoints?.largeDesktop ?? breakPoints.largeDesktop

  return (
    <ConfigProvider locale={ruRu}>
        <ContextProvider
            value={{
              Link,
              responsiveModel: {
                width,
                isTouch: isSafe(isTouch) ? isTouch : false,
                isMobile: isSafe(isMobile) ? isMobile : width < mobile,
                isTablet: isSafe(isTablet) ? isTablet : width >= mobile && width < tablet,
                isTabletAndPhone: isSafe(isTabletAndPhone) ? isTabletAndPhone : width < tablet,
                isBeforeDesktop: isSafe(isBeforeDesktop) ? isBeforeDesktop : width < desktop,
                isDesktop: isSafe(isDesktop) ? isDesktop : width >= tablet,
                isMiddleDesktop: isSafe(isMiddleDesktop) ? isMiddleDesktop : width >= desktop &&
                  width < largeDesktop,
                isLargeDesktop: isSafe(isLargeDesktop) ? isDesktop : width >= largeDesktop,
                breakPoints: {
                  mobile,
                  tablet,
                  desktop,
                  largeDesktop
                }
              },
              remPoint,
              label
            }}
        >
            <GlobalStyles/>
            {children}
        </ContextProvider>
    </ConfigProvider>
  )
}

Provider.propTypes = {
  width: PropTypes.number.isRequired,
  responsiveModel: PropTypes.object
}

export default Provider
