import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import Block from 'ui/Block'
import Icon from 'ui/Icon'
import styled from 'styled-components'
import { colors } from 'styles'
import { isSafe, safeObject } from 'utils/object'
import { Context } from '../../Provider/Context'

const Button = styled(Block)`
  font-weight: 500;
  transition: all .3s ease-in-out !important;
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 32px;
  height: 32px;
  border-radius: 7px;
  user-select: none;
  cursor: ${props => (props.isDisable ? 'not-allowed' : 'pointer')};
  color: ${props => (!props.isDisable ? colors.gray4 : colors.gray3)};
  background: ${colors.gray1};
  z-index: 2;
  
  &:hover {
    color: ${props => (!props.isDisable ? colors.primary6 : colors.gray3)};
  }
  
  & * {
    transition: none;
  }
`

const MoreDecorate = styled(Block)`
  width: 6px;
  height: 25px;
  background-color: ${colors.gray2};
  z-index: 1;
`

const MoreDecorateDelimiter = styled(Block)`
  width: 3px;
  height: 25px;
  background-color: #fff;
  z-index: 1;
`

const MoreDecoratePrev = styled(MoreDecorate)`
  transform: translateX(35%);
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
`

const MoreDecorateDelimiterPrev = styled(MoreDecorateDelimiter)`
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  transform: translateX(-0.5px);
  margin-right: -1px;
`

const MoreDecorateDelimiterNext = styled(MoreDecorateDelimiter)`
  border-top-right-radius: 20px;
  border-bottom-right-radius: 20px;
  transform: translateX(0.5px);
  margin-left: -1px;
`

const MoreDecorateNext = styled(MoreDecorate)`
  transform: translateX(-35%);
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  z-index: 0;
`

const PageNumber = styled(Block)`
  background: ${props => (!props.isBackground ? !props.isDisable ? colors.primary1 : colors.gray1 : 'transparent')};
  color: ${props => (!props.isDisable ? colors.primary6 : colors.gray3)};
  min-width: 28px;
  padding: 0 14px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 7px;
  font-weight: 600;
  margin-left: 7px;
  margin-right: 7px;
  user-select: none;
`

const LazyPagination = ({ page, onChange, isDisable, data, pageSize, htmlID }) => {
  const { responsiveModel: { isTabletAndPhone } } = useContext(Context)

  const handleChange = ({ count, forcePage = null }) => () => {
    if (isSafe(forcePage) && onChange) onChange(forcePage)
    else if (onChange) onChange(page + count)
  }

  const navPage = type => {
    const isDisableButton = (type === 'prev' && page <= 10) || (type === 'next' && data.length < pageSize)
    return (
        <Block horizontalPlacement="left" verticalPlacement="center">
          {type === 'prev' && (
          <>
            <MoreDecoratePrev/>
            <MoreDecorateDelimiterPrev/>
          </>
          )}
          <Button
              size="sm"
              width={3}
              marginRight={type === 'prev' ? 0.15 : 0}
              marginLeft={type === 'next' ? 0.15 : 0}
              onClick={!isDisable && (!isDisableButton) ? handleChange({ count: type === 'prev' ? -10 : 10 }) : null}
              isDisable={isDisableButton || isDisable}
              {...safeObject({ htmlID: htmlID ? `${htmlID}-pagination-${type}-10` : null })}
          >
            {type === 'prev' ? '-10' : '+10'}
          </Button>
            {type === 'next' && (
            <>
                <MoreDecorateDelimiterNext/>
                <MoreDecorateNext/>
            </>
            )}
        </Block>
    )
  }

  const nav = type => {
    const isDisableButton = (type === 'prev' && page <= 1) || (type === 'next' && data.length < pageSize)
    return (
          <Button
              size="sm"
              width={3}
              onClick={!isDisable && (!isDisableButton) ? handleChange({ count: type === 'prev' ? -1 : 1 }) : null}
              isDisable={isDisableButton || isDisable}
              {...safeObject({ htmlID: htmlID ? `${htmlID}-pagination-${type}-1` : null })}
          >
              <Icon
                  icon="chevron-down"
                  iconType="redesign"
                  rotate={type === 'prev' ? 90 : 270}
                  size={1.2}
              />
          </Button>
    )
  }

  return (
    <Block horizontalPlacement="left" marginRight={!isTabletAndPhone ? -2 : 0}>
        {!isTabletAndPhone &&
        <Button
            size="sm"
            width={3}
            isMarginRight marginRight={2}
            onClick={!isDisable && page !== 1 ? handleChange({ forcePage: 1 }) : null}
            isDisable={isDisable || page === 1}
            {...safeObject({ htmlID: htmlID ? `${htmlID}-pagination-home` : null })}
        >
            1
        </Button>
        }
        {navPage('prev')}
        {nav('prev')}
        <PageNumber
            isBackground={isTabletAndPhone}
            isDisable={isDisable}
            {...safeObject({ htmlID: htmlID ? `${htmlID}-pagination-number` : null })}
        >
            {page}
        </PageNumber>
        {nav('next')}
        {navPage('next')}
    </Block>
  )
}

LazyPagination.propTypes = {
  htmlID: PropTypes.any,
  page: PropTypes.number,
  isDisable: PropTypes.bool,

  onChange: PropTypes.func,
  data: PropTypes.array,
  pageSize: PropTypes.number
}

LazyPagination.defaultProps = {
  isDisable: false
}

export default LazyPagination
