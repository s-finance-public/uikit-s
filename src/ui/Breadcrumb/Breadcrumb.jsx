import _ from 'lodash'
import React from 'react'
import Icon from 'ui/Icon'
import Block from 'ui/Block'
import PropTypes from 'prop-types'
import { colors } from 'styles'
import withMargin from 'ui/common/withMargin'
import AntBreadCrumb from 'antd/es/breadcrumb'
import styled from 'styled-components'

const Root = styled(AntBreadCrumb)`
  color: ${colors.text} !important;
  user-select: none;
  
  & .ant-breadcrumb-separator {
    margin: 0 4px;
  }
`

const Item = styled(AntBreadCrumb.Item)`
  display: inline-block;
`

const Underline = styled.div`
  width: 104%;
  height: 2px;
  border-radius: 1px;
  background: ${colors.primary};
  margin-top: -2px;
  margin-left: -2%;
`

const Breadcrumb = ({ className, style, options }) => (
    <Root
        className={className}
        style={style}
        separator={
            <Icon
                icon="chevron-down"
                iconType="redesign"
                rotate={270}
                color="gray4"
                size={16}
                isRem={false}
                style={{ transform: 'translateY(2px)' }}
            />
        }
    >
        {_.map(options, (option, index) => (
            <Item
                key={index}
            >
                <Block onClick={option.onClick}>
                    {option.node}
                    {option.isUnderline && <Underline/>}
                </Block>
            </Item>
        ))}
    </Root>
)

Breadcrumb.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  options: PropTypes.arrayOf(PropTypes.shape({
    node: PropTypes.any,
    isUnderline: PropTypes.bool,
    onClick: PropTypes.func
  }))
}

export default withMargin({ displayName: 'Breadcrumb' })(Breadcrumb)
