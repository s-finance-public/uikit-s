import React, { useState, useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'
import AntdSwitch from 'antd/es/switch'
import Block from 'ui/Block'
import { Paragraph } from 'ui/Typography'
import withFormProps from 'ui/common/withFormProps'
import withMargin from 'ui/common/withMargin'
import styled from 'styled-components'
import Icon from 'ui/Icon'
import { colors } from 'styles'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const LabelContainer = styled(Paragraph)`
  user-select: none;
  transition: color .3s ease-in-out;
  line-height: 1.7;
  margin-bottom: 0 !important;
  
  &:hover {
    color: ${colors.primary6} !important;
  }
`

const UiSwitch = styled(AntdSwitch)`
  display: inline-block;
  margin: 0 8px 0 0 !important;
`

const Switch = ({
  className, style, htmlID, dataSet,

  isLoading, isDisable, isCheckSuffix, isUncheckSuffix,
  tabIndex,
  onChange, children,

  ...props
}) => {
  const [isChecked, setIsChecked] = useState(false)

  const handleChange = v => {
    setIsChecked(v)
    if (onChange) onChange(v)
  }

  useEffect(() => {
    setIsChecked(props.isChecked)
  }, [props.isChecked])

  return (
      <Block
        className={className}
        style={style}
      >
        <Block horizontalPlacement="left">
          <UiSwitch
              className={className}
              style={safeObject({
                cursor: isDisable ? 'not-allowed' : 'pointer'
              })}
              checked={isChecked}
              loading={isLoading}
              disabled={isDisable}
              {...safeObject({
                tabIndex,
                checkedChildren: isCheckSuffix,
                unCheckedChildren: isUncheckSuffix,
                id: htmlID || null
              })}
              {...applyHtmlData(dataSet)}
              onClick={!isDisable ? () => handleChange(!isChecked) : null}
          />
          {children &&
          <LabelContainer
            style={{
              cursor: isDisable ? 'not-allowed' : 'pointer'
            }}
            onClick={!isDisable ? () => handleChange(!isChecked) : null}
          >
            {children}
          </LabelContainer>
          }
      </Block>
    </Block>
  )
}

Switch.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  isChecked: PropTypes.bool,
  isLoading: PropTypes.bool,
  isDisable: PropTypes.bool,
  isCheckSuffix: PropTypes.any,
  isUncheckSuffix: PropTypes.any,
  tabIndex: PropTypes.number,

  onChange: PropTypes.func
}

Switch.defaultProps = {
  isChecked: false,
  isLoading: false,
  isDisable: false,
  isCheckSuffix: <Icon icon="check" iconType="design" size={9.8} isRem={false}/>,
  isUncheckSuffix: <Icon icon="close" iconType="design" size={12} isMarginTop marginTop={4.5} isRem={false} color="gray4"/>
}

Switch.displayName = 'Switch'

export default withMargin({ displayName: 'Switch' })(withFormProps({
  isOnlyShowForceValidate: true,
  displayName: 'Switch'
})(Switch))
