import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import Icon from 'ui/Icon'
import Switch from './Switch'

const stories = storiesOf('Switch', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
    <Switch/>
  ))
  .add('loading', () => (
    <Switch isLoading/>
  ))
  .add('disabled', () => (
    <Switch isDisable/>
  ))
  .add('changed', () => (
    <Switch onChange={action('changed')}/>
  ))
  .add('with children', () => (
    <Switch>Children</Switch>
  ))
  .add('with custom suffix', () => (
    <Switch
      onChange={action('changed')}
      isCheckSuffix={<Icon icon="eye" />}
      isUncheckSuffix={<Icon icon="edit" />}
    />
  ))

storiesHelpers({
  stories,
  Component: props => <Switch {...props}/>,
  isOnlyForceValidation: true,
  isWidth: false
})
