import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Button from 'ui/Button'
import confirm from 'utils/confirm'
import { Paragraph } from 'ui/Typography'

const stories = storiesOf('Confirm', module)

const ExampleContainer = () => (
    <Fragment>
        <Paragraph>One</Paragraph>
        <Paragraph>Two</Paragraph>
    </Fragment>
)

stories
  .add('default', () => (
    <Button
        onClick={() => confirm({
          title: 'Default confirm',
          content: <ExampleContainer/>,
          onOk: action('onOk'),
          onCancel: action('onCancel')
        })}
    >
        Default
    </Button>
  ))
  .add('centered', () => (
    <Button
        onClick={() => confirm({
          title: 'Centered confirm',
          content: <ExampleContainer/>,
          isCenter: true,
          onOk: action('onOk'),
          onCancel: action('onCancel')
        })}
    >
        Centered confirm
    </Button>
  ))
  .add('with custom width', () => (
    <Button
        onClick={() => confirm({
          title: 'Confirm with custom width',
          content: <ExampleContainer/>,
          width: '60%',
          onOk: action('onOk'),
          onCancel: action('onCancel')
        })}
    >
        Confirm with custom width
    </Button>
  ))
  .add('with custom icon', () => (
    <Button
        onClick={() => confirm({
          title: 'Confirm with custom icon',
          content: <ExampleContainer/>,
          icon: 'logout',
          onOk: action('onOk'),
          onCancel: action('onCancel')
        })}
    >
        Confirm with custom icon
    </Button>
  ))
  .add('with custom ok and cancel', () => (
    <Button
        onClick={() => confirm({
          title: 'Confirm with custom ok and cancel',
          content: <ExampleContainer/>,
          okText: 'Хорошо',
          okType: 'danger',
          cancelText: 'Вернуться',
          onOk: action('onOk'),
          onCancel: action('onCancel')
        })}
    >
        Confirm with custom ok and cancel
    </Button>
  ))
