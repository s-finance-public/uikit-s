import React from 'react'
import PropTypes from 'prop-types'
import AntProgress from 'antd/es/progress'
import withMargin from 'ui/common/withMargin'
import { colors } from 'styles'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Progress = ({
  className, style, htmlID, dataSet,

  type, percent, format, strokeColor, strokeLinecap, isShowInfo, strokeWidth, status, width
}) => (
    <AntProgress
        className={className}
        style={style}
        type={type}
        percent={percent}
        strokeLinecap={strokeLinecap}
        showInfo={isShowInfo}
        strokeWidth={strokeWidth}
        {...safeObject({
          format,
          strokeColor,
          status,
          width: type === 'circle' || type === 'dashboard' ? width : null,
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
    />
)

Progress.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  percent: PropTypes.number,
  type: PropTypes.oneOf(['linear', 'circle', 'dashboard']),
  strokeWidth: Progress.number,
  strokeColor: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  strokeLinecap: PropTypes.oneOf(['round', 'square']),
  isShowInfo: PropTypes.bool,
  status: PropTypes.oneOf(['success', 'exception', 'normal', 'active']),
  width: PropTypes.number,

  format: PropTypes.func
}

Progress.defaultProps = {
  percent: 0,
  type: 'line',
  format: null,
  strokeWidth: 10,
  strokeColor: colors.primary,
  strokeLinecap: 'round',
  isShowInfo: true,
  status: null,
  width: 132
}

Progress.displayName = 'Progress'

export default withMargin({ isWidth: false, displayName: 'Progress' })(Progress)
