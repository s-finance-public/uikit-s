import _ from 'lodash'
import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Icon from 'ui/Icon'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import Divider from 'ui/Typography/Divider'
import { moneyRubNormalizer, moneyRubDenormalizer } from 'utils/formatters/money'
import Input from './Input'

const stories = storiesOf('Input', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
      {story()}
    </div>
))

stories
  .add('default', () => (
    <Input/>
  ))
  .add('with clear', () => (
    <Input isAllowClear/>
  ))
  .add('text align', () => (
    <Fragment>
        <Input isMarginBottom/>
        <Input textAlign="center" isMarginBottom/>
        <Input textAlign="right"/>
    </Fragment>
  ))
  .add('with placeholder', () => (
    <Input placeholder="Edit text"/>
  ))
  .add('with left', () => (
    <Input label="Count" left="Sum"/>
  ))
  .add('with right', () => (
    <Input label="Count" right={<Icon icon="info-circle"/>}/>
  ))
  .add('with left & right', () => (
    <Input
        label="Count"
        left="Sum"
        right={<Icon icon="info-circle"/>}
        onClick={action('input click')}
        onClickLeft={action('left click')}
        onClickRight={action('right click')}
    />
  ))
  .add('with prefix', () => (
    <Input label="Count" prefix="$"/>
  ))
  .add('with postfix', () => (
    <Input
        label="Count"
        postfix="₽"
    />
  ))
  .add('with prefix & postfix', () => (
    <Input label="Count" prefix="$" postfix="₽"/>
  ))
  .add('with prefix & postfix && left && right', () => (
    <Input
      label="Count"
      prefix="$"
      postfix="₽"
      left="Sum"
      right={<Icon icon="info-circle"/>}
    />
  ))
  .add('sizes', () => (
    <Fragment>
        <Input
            label="Count"
            isVerticalLabel={false}
            size="xs"
            isMarginBottom
        />
        <Input
            label="Count"
            isVerticalLabel={false}
            size="sm"
            isMarginBottom
        />
        <Input
            label="Count"
            isVerticalLabel={false}
            size="lg"
            isMarginBottom
        />
        <Input
            label="Count"
            size="xs"
            isMarginBottom
        />
        <Input
            label="Count"
            size="sm"
            isMarginBottom
        />
        <Input
            label="Count"
            size="lg"
        />
    </Fragment>
  ))
  .add('disabled', () => (
    <Input isDisable/>
  ))
  .add('onMount', () => (
    <Input onMount={target => target.focus()}/>
  ))

storiesHelpers({
  stories,
  Component: props => (
    <Input
        prefix="$"
        postfix="₽"
        left="Sum"
        right={<Icon icon="info-circle"/>}
        {...props}
    />
  )
})

stories
  .add('number with max min', () => (
    <Input label="Count" min={10} max={500} type="number"/>
  ))
  .add('number with max, min, slider, step', () => (
    <Input label="Count" min={10} max={500} step={20} type="number" isWithSlider/>
  ))
  .add('phone number', () => (
      <Input
          label="Мобильный телефон"
          placeholder="(999) 999-9999"
          onChange={action('any value')}
          prefix={<span>+7 </span>}
          mask="(999) 999-9999"
          unmask={v => v.replace(/[\s()-]*/gi, '')}
          type="number"
      />
  ))

stories
  .add('number normalizer & denormalizer', () => (
    <Fragment>
        {_.map(_.map(['xs', 'sm', 'lg'], size => (
            <Fragment key={size}>
                <Input
                    label="Count"
                    type="number"
                    normalizer={moneyRubNormalizer}
                    denormalizer={moneyRubDenormalizer}
                    size={size}
                />
                <Input
                    label="Count"
                    type="number"
                    textAlign="center"
                    normalizer={moneyRubNormalizer}
                    denormalizer={moneyRubDenormalizer}
                    size={size}
                />
                <Input
                    label="Count"
                    type="number"
                    textAlign="right"
                    normalizer={moneyRubNormalizer}
                    denormalizer={moneyRubDenormalizer}
                    size={size}
                />
                <Divider/>
                <Input
                    type="number"
                    normalizer={moneyRubNormalizer}
                    denormalizer={moneyRubDenormalizer}
                    size={size}
                    isMarginBottom
                />
                <Input
                    type="number"
                    textAlign="center"
                    normalizer={moneyRubNormalizer}
                    denormalizer={moneyRubDenormalizer}
                    size={size}
                    isMarginBottom
                />
                <Input
                    type="number"
                    textAlign="right"
                    normalizer={moneyRubNormalizer}
                    denormalizer={moneyRubDenormalizer}
                    size={size}
                    isMarginBottom
                />
            </Fragment>
        )), n => (
            <Fragment>
                {n}
                <Divider/>
            </Fragment>
        ))}
    </Fragment>
  ))

stories
  .add('disabled', () => (
    <Input label="Disabled" isDisable/>
  ))
  .add('textarea', () => (
    <Input label="Textarea" isTextArea/>
  ))
  .add('textarea readonly', () => (
    <Input label="Textarea Readonly" isTextArea isReadonly/>
  ))
  .add('textarea with placeholder', () => (
    <Input label="Textarea with placeholder" isTextArea placeholder="Edit text"/>
  ))
  .add('textarea without autosize', () => (
    <Input label="Textarea without autosize" isTextArea isAutoSize={false} minRows={3} maxRows={10}/>
  ))
  .add('with mask', () => (
    <Input
        label="Mask" mask="99.99.99"
        placeholder="Input date"
        prefix="$"
        postfix="₽"
    />
  ))
  .add('with onChange', () => (
    <Input onChange={action('any value')}/>
  ))
