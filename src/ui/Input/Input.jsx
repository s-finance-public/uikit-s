import _ from 'lodash'
import React, { useState, useEffect, useRef, useContext, forwardRef } from 'react'
import PropTypes from 'prop-types'
import AntdInput from 'antd/es/input'
import withFormProps from 'ui/common/withFormProps'
import withMargin from 'ui/common/withMargin'
import Slider from 'ui/Slider'
import { v4 as uuid } from 'uuid'
import { mountHook } from 'hooks/mount'
import { colors } from 'styles'
import { Context } from 'ui/Provider/Context'
import styled from 'styled-components'
import { safeObject, isSafe } from 'utils/object'
import InputComponent from './InputComponent'
import MaskInput from './MaskInput'

const Root = styled(forwardRef((props, ref) => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isDisable')
  return <div ref={ref} {...resolveProps} />
}))`
  & input {
    border-radius: 8px;
  }

  & input:-webkit-autofill {
    background-color: ${props => (!props.isDisable ? '#fff' : '#f5f5f5')} !important;
    background: ${props => (!props.isDisable ? '#fff' : '#f5f5f5')} !important;
    border-color: #d9d9d9 !important;
    border-right-width: 1px !important;
    -webkit-text-fill-color: ${props => (!props.isDisable ? colors.text : 'rgba(0,0,0,0.25)')} !important;
    -webkit-box-shadow: 0 0 0px 1000px transparent inset !important;
    transition: background-color 5000s ease-in-out 0s !important;
  }

  &:hover {
    background-color: ${props => (!props.isDisable ? '#fff' : '#f5f5f5')} !important;
    background: ${props => (!props.isDisable ? '#fff' : '#f5f5f5')} !important;
    border-right-width: 1px !important;
    -webkit-box-shadow: 0 0 0px 1000px transparent inset !important;
    transition: background-color 5000s ease-in-out 0s !important;
    border-color: ${props => (!props.isDisable ? '#6bd3ff' : ' #d9d9d9;')} !important;
  }

  &:focus, &:active & input:-webkit-autofill,
  & input:-webkit-autofill:hover,
  & input:-webkit-autofill:focus,
  & input:-webkit-autofill:active  {
    background-color: ${props => (!props.isDisable ? '#fff' : '#f5f5f5')} !important;
    background: ${props => (!props.isDisable ? '#fff' : '#f5f5f5')} !important;
    border-right-width: 1px !important;
    -webkit-text-fill-color: ${props => (!props.isDisable ? colors.text : 'rgba(0,0,0,0.25)')} !important;
    -webkit-box-shadow: 0 0 0px 1000px transparent inset !important;
    transition: background-color 5000s ease-in-out 0s !important;
    border-color: ${props => (!props.isDisable ? '#6bd3ff' : '#d9d9d9')} !important;
  }
`

const Input = ({
  className, style, htmlID, dataSet,

  type, min, max,
  size, value, placeholder,
  left, right, prefix, postfix,
  isDisable, isReadonly,
  tabIndex,
  isTextArea, isAutoSize, minRows, maxRows, step, isWithSlider,
  mask, maskChar, unmask,
  textAlign, normalizer, denormalizer, sliderNormalizer, isAllowClear,

  isCommonInput, isForceChange, isForceLastCaretFocus,

  internalUnmask, name, isAutoComplete, isSmsCode,

  onFocus, onMetaFocus, onBlur, onMetaBlur, onChange, onPressEnter, onKeyDown, onKeyUp, onKeyPress,
  onUp, onDown, onClick, onClickLeft, onClickRight, onClear,

  onChangeIsInput, onChangeIsFocus,
  onMount
}) => {
  const rootRef = useRef(null)
  const [currentValue, setValue] = useState(value)
  const [isInput, setIsInput] = useState(false)
  const [isFocus, setIsFocus] = useState(false)
  const [htmlUniqID, setHtmlUniqID] = useState(null)
  const { responsiveModel } = useContext(Context)

  mountHook(() => {
    const ID = htmlID || uuid()
    setHtmlUniqID(ID)

    if (onMount) {
      onMount({
        focus: () => rootRef?.current?.querySelector('input').focus(),
        blur: () => rootRef?.current?.querySelector('input').blur()
      })
    }
  })

  const handleChange = (isChangeOutside = true) => e => {
    const c = e && e.target ? e.target.value : e
    let v = denormalizer ?
      denormalizer(_.isNumber(c) || _.isString(c) ? c.toString() : '') : c
    if (type === 'number' && !mask) {
      v = _.isNaN(Number(v)) ? min : v > max ? max : v
    }

    if (_.isObject(v)) return

    const maskRegexp = new RegExp(`${maskChar}*`, 'gi')
    const outsideValue = mask ? (unmask ? unmask(_.isString(v) ? v : '') : v || '').replace(maskRegexp, '') : v
    if (normalizer) {
      v = normalizer(mask ? (unmask ? unmask(_.isString(v) ? v : '') : v || '').replace(maskRegexp, '') : v)
    }

    if (isChangeOutside) {
      setIsInput(true)
      if (onChangeIsInput) onChangeIsInput(true)

      if (onChange) {
        onChange(!isCommonInput ? outsideValue : { target: { value: outsideValue } })
      }
    }

    setValue(v)
  }

  const handleFocus = () => {
    setIsFocus(true)
    if (onChangeIsFocus) onChangeIsFocus(true)
    if (onFocus) onFocus()
    if (onMetaFocus) onMetaFocus()
    if ((isForceLastCaretFocus || (mask && !isForceLastCaretFocus)) && isSafe(currentValue) && currentValue !== '') {
      const elem = document.getElementById(`${htmlUniqID}`)
      const caretPos = currentValue.length - 1

      if (elem != null) {
        if (elem.createTextRange) {
          const range = elem.createTextRange()
          range.move('character', caretPos)
          range.select()
        } else if (elem.selectionStart) {
          elem.focus()
          elem.setSelectionRange(caretPos, caretPos)
        } else elem.focus()
      }
    }
  }

  const handleBlur = () => {
    setIsFocus(false)
    if (type === 'number' && !mask && (Number(currentValue) < min || currentValue === '')) {
      setValue(min)
      if (onChange) onChange(min)
    }

    if (onChangeIsInput) onChangeIsInput(true)
    if (onChangeIsFocus) onChangeIsFocus(false)
    if (onBlur) onBlur()
    if (onMetaBlur) onMetaBlur()
  }

  useEffect(() => {
    if (!isFocus || isForceChange) {
      handleChange(false)(value)
    }
  }, [value])

  const handleKeyDown = e => {
    switch (e.keyCode) {
      case 40:
        if (type === 'number' && !mask) handleChange()(_.isNumber(Number(value)) ? Number(value) - step < min ? min : Number(value) - step : min)
        if (onDown) onDown()
        break
      case 38:
        if (type === 'number' && !mask) handleChange()(_.isNumber(Number(value)) ? Number(value) + step > max ? max : Number(value) + step : max)
        if (onUp) onUp()
        break
    }
    if (onKeyDown) onKeyDown(e)
  }

  const handleKeyUp = e => {
    if (onKeyUp) onKeyUp(e)
  }

  const inputProps = safeObject({
    className,
    style: { width: '100%' },
    type,
    isTouch: responsiveModel.isTouch,
    textAlign,
    value: currentValue,
    isDisable,
    isReadonly,
    placeholder: !mask ? placeholder : null,
    onFocus: handleFocus,
    onBlur: handleBlur,
    onChange: handleChange(),
    onPressEnter: !isCommonInput ? onPressEnter : null,
    onKeyDown: handleKeyDown,
    onKeyUp: handleKeyUp,
    onKeyPress,
    size,
    left,
    right,
    prefix,
    postfix,
    min,
    max,
    tabIndex,
    htmlID: htmlUniqID,
    dataSet,
    onClickLeft: e => {
      e.stopPropagation()
      e.preventDefault()
      if (onClickLeft) onClickLeft()
    },
    onClickRight: e => {
      e.stopPropagation()
      e.preventDefault()
      if (onClickRight) onClickRight()
    },
    onClick,
    internalUnmask,
    mask,
    name: name || null,
    isAutoComplete,
    isSmsCode,
    isAllowClear,
    onClear
  })

  const denormalizerValue = denormalizer && currentValue !== '' ?
    denormalizer(_.isNumber(currentValue) || _.isString(currentValue) ? currentValue.toString() : '') :
    currentValue

  return (
    !isTextArea ?
      <Root
        style={{ width: '100%', ...style }}
        isDisable={isDisable}
        ref={rootRef}
      >
        {!mask ? <InputComponent {...inputProps} /> :
          <MaskInput
            mask={mask}
            maskChar={maskChar}
            placeholder={placeholder}
            Input={InputComponent}
            {...inputProps}
          />
        }
        {isWithSlider && type === 'number' &&
          <Slider
            value={Number(denormalizerValue) >= min || Number(denormalizerValue) <= max ?
              Number(denormalizerValue) : min}
            min={Number(min) || 0}
            max={Number(max) > Number(min) ? Number(max) : 1}
            step={Number(max) > step ? step : 1}
            isDisable={isDisable}
            onChange={handleChange()}
            {...safeObject({
              tipFormatter: sliderNormalizer || null
            })}
          />
        }
      </Root> :
      <AntdInput.TextArea
        autosize={isAutoSize || { minRows, maxRows }}
        style={isReadonly ? { backgroundColor: 'white', color: 'black', width: '100%' } : { width: '100%' }}
        value={currentValue}
        onChange={handleChange()}
        onFocus={handleFocus}
        onBlur={handleBlur}
        disabled={isDisable || isReadonly}
        onPressEnter={onPressEnter}
        placeholder={placeholder}
        {...safeObject({
          tabIndex,
          rows: !isAutoSize ? maxRows : null
        })}
      />
  )
}

Input.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  value: PropTypes.any,
  left: PropTypes.any,
  right: PropTypes.any,
  prefix: PropTypes.any,
  postfix: PropTypes.any,
  type: PropTypes.string,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  min: PropTypes.number,
  max: PropTypes.number,
  placeholder: PropTypes.string,
  isDisable: PropTypes.bool,
  isTextArea: PropTypes.bool,
  isAutoSize: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  minRows: PropTypes.number,
  maxRows: PropTypes.number,
  step: PropTypes.number,
  isWithSlider: PropTypes.bool,
  textAlign: PropTypes.oneOf(['left', 'center', 'right']),
  mask: PropTypes.string,
  maskChar: PropTypes.string,
  unmask: PropTypes.func,

  onFocus: PropTypes.func,
  onMetaFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onMetaBlur: PropTypes.func,
  onChange: PropTypes.func,
  onPressEnter: PropTypes.func,
  onKeyDown: PropTypes.func,
  onKeyUp: PropTypes.func,
  onKeyPress: PropTypes.func,
  onClick: PropTypes.func,
  onUp: PropTypes.func,
  onDown: PropTypes.func,
  normalizer: PropTypes.func,
  denormalizer: PropTypes.func,
  sliderNormalizer: PropTypes.func,

  isCommonInput: PropTypes.bool,
  isForceChange: PropTypes.bool,
  isForceLastCaretFocus: PropTypes.bool,

  internalUnmask: PropTypes.func,
  isAllowClear: PropTypes.bool,

  onClickLeft: PropTypes.func,
  onClickRight: PropTypes.func,
  onClear: PropTypes.func,

  tabIndex: PropTypes.any,
  name: PropTypes.string,
  isAutoComplete: PropTypes.bool,
  isSmsCode: PropTypes.bool,

  // withFormProps
  onChangeIsInput: PropTypes.func,
  onChangeIsFocus: PropTypes.func,

  onMount: PropTypes.func
}

Input.defaultProps = {
  value: '',
  left: null,
  right: null,
  prefix: null,
  postfix: null,
  type: 'text',
  size: 'lg',
  min: 0,
  max: 10000000,
  placeholder: '',
  isDisable: false,
  isTextArea: false,
  isAutoSize: true,
  minRows: 2,
  maxRows: 6,
  step: 1,
  isWithSlider: false,
  textAlign: 'left',
  mask: '',
  maskChar: '_',
  unmask: null,

  isCommonInput: false,
  isForceChange: false,
  isForceLastCaretFocus: false,

  internalUnmask: null,
  isAllowClear: false,

  tabIndex: null,

  isAutoComplete: false,
  isSmsCode: false
}

const InputGroup = withMargin()(AntdInput.Group)

Input.displayName = 'Input'
InputGroup.displayName = 'InputGroup'

export { InputGroup }

export default withMargin({ displayName: 'Input' })(withFormProps({ displayName: 'Input' })(
  React.forwardRef((props, ref) => <Input {...props}/>)
))
