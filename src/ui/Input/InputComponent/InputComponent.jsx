import _ from 'lodash'
import React from 'react'
import styled from 'styled-components'
import AntdInput from 'antd/es/input'
import { Text } from 'ui/Typography'
import Block from 'ui/Block'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const InputContainer = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'textAlign')
  return <AntdInput {...resolveProps}/>
})`
  text-align: ${props => `${props.textAlign} !important`};
  
  & .ant-input-group-addon {
    padding: 0;
  }
  
  & input {
    text-align: ${props => `${props.textAlign} !important`};
  }
`

const Addon = styled.span`
  position: relative;
  padding: 0 11px;
  color: rgba(0,0,0,0.9);
  font-weight: normal;
  font-size: 14px;
  line-height: 1;
  text-align: center;
  background-color: transparent;
  transition: all .3s;
`

const InputComponent = ({
  className, style, htmlID, dataSet,
  textAlign, value, isDisable, placeholder,
  size, left, right, prefix, postfix, type, tabIndex,
  min, max,
  onFocus, onBlur, onChange, onPressEnter, onKeyDown, onKeyUp, onKeyPress,
  onClick, onClickLeft, onClickRight, onClear, isAllowClear,

  internalUnmask, mask, name, isAutoComplete, isSmsCode,

  isTouch
}) => {
  const clearPostfix = (
      <Block color="gray3" style={{ marginTop: '4px' }}>
        <svg
            viewBox="64 64 896 896"
            focusable="false"
            className=""
            data-icon="close-circle"
            width="1em"
            height="1em"
            fill="currentColor" aria-hidden="true"
            onClick={e => {
              e.stopPropagation()
              e.preventDefault()

              if (onClear) onClear()
              else onChange('')
            }}
        >
            <path
                d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm165.4 618.2l-66-.3L512 563.4l-99.3 118.4-66.1.3c-4.4 0-8-3.5-8-8 0-1.9.7-3.7 1.9-5.2l130.1-155L340.5 359a8.32 8.32 0 0 1-1.9-5.2c0-4.4 3.6-8 8-8l66.1.3L512 464.6l99.3-118.4 66-.3c4.4 0 8 3.5 8 8 0 1.9-.7 3.7-1.9 5.2L553.5 514l130 155c1.2 1.5 1.9 3.3 1.9 5.2 0 4.4-3.6 8-8 8z"
            />
        </svg>
      </Block>
  )

  const cleanValue = mask ? internalUnmask ? internalUnmask(value || '') : value || (value || '').replace(/_/ig, '') : value

  return (
    <InputContainer
        className={className}
        style={{ width: '100%', ...style }}
        textAlign={textAlign}
        value={value}
        disabled={isDisable}
        placeholder={placeholder}
        allowClear={false}
        onFocus={onFocus}
        onBlur={onBlur}
        onChange={onChange}
        onKeyDown={onKeyDown}
        onKeyUp={onKeyUp}
        onKeyPress={onKeyPress}
        name={name || 'unnamed-field'}
        onClick={onClick}
        {...safeObject({
          autoComplete: (() => {
            if (isSmsCode) return 'one-time-code'
            return !isAutoComplete ? 'off' : 'on'
          })(),
          size: size === 'lg' ? 'large' : size === 'xs' ? 'small' : null,
          addonBefore: left ? <Addon onClick={onClickLeft}>{left}</Addon> : null,
          addonAfter: right ? <Addon onClick={onClickRight}>{right}</Addon> : null,
          prefix: prefix ? <Text type="gray4">{prefix}</Text> : null,
          suffix: (postfix || isAllowClear) ? (
              <Block horizontalPlacement="left" verticalPlacement="center" color="gray4">
                  {!isDisable && cleanValue !== '' && isAllowClear && clearPostfix}
                  {postfix &&
                  <Block style={{ marginLeft: '6px' }}>
                      {postfix}
                  </Block>
                  }
              </Block>
          ) : null,
          tabIndex: !_.isBoolean(tabIndex) ? tabIndex : null,
          min: type === 'number' ? min : null,
          max: type === 'number' ? max : null,
          type: type === 'number' ? !isTouch ? 'text' : 'tel' : type,
          id: htmlID || null,
          onPressEnter: onPressEnter || null
        })}
        {...applyHtmlData(dataSet)}
        noValidate
        formNoValidate
    />
  )
}

export default InputComponent
