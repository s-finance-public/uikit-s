import fp from 'lodash/fp'
import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { Divider } from 'ui/Typography'
import Block from 'ui/Block'
import Button, { ButtonGroup } from './Button'

const stories = storiesOf('Button', module)

fp.forEach(type => {
  stories.add(`type: ${type}`, () => (
    <Button type={type}>
        Button
    </Button>
  ))
})(['default', 'primary', 'danger'])

stories
  .add('loading', () => (
    <Fragment>
        <Block horizontalPlacement="left" isMarginBottom>
            <Button isMarginRight>
                Button
            </Button>
            <Button isLoading>
                Button
            </Button>
        </Block>
        <Divider/>
        <Block horizontalPlacement="left" isMarginBottom>
            <Button type="primary" isMarginRight>
                Button
            </Button>
            <Button type="primary" isLoading>
                Button
            </Button>
        </Block>
        <Divider/>
        <Block horizontalPlacement="left" isMarginBottom>
            <Button type="danger" isMarginRight>
                Button
            </Button>
            <Button type="danger" isLoading>
                Button
            </Button>
        </Block>
    </Fragment>
  ))
  .add('sizes', () => (
    <Fragment>
        <Button type="primary" size="xs" isMarginBottom>
            Button
        </Button>
        <Divider/>
        <Button type="primary" size="sm" isMarginBottom>
            Button
        </Button>
        <Divider/>
        <Button type="primary" size="lg">
            Button
        </Button>
    </Fragment>
  ))
  .add('without fill', () => (
    <Block horizontalPlacement="left">
        <Button type="primary" isFill={false} isMarginLeft>
            Button
        </Button>
        <Button type="danger" isFill={false} isMarginLeft>
            Button
        </Button>
    </Block>
  ))
  .add('with left icon', () => (
    <Button
        type="primary"
        icon="eye"
    >
        Button
    </Button>
  ))
  .add('with right icon', () => (
    <Button
        type="primary"
        icon="edit"
        iconPlacement="right"
    >
        Button
    </Button>
  ))
  .add('with loading', () => (
    <Button
        type="primary"
        isLoading
    >
        Button
    </Button>
  ))
  .add('with left', () => (
    <Button
        type="primary"
        left="&"
    >
        Button
    </Button>
  ))
  .add('with right', () => (
    <Button
        type="primary"
        right="$"
    >
        Button
    </Button>
  ))
  .add('with left & right', () => (
    <Button
        type="primary"
        left="&"
        right="$"
    >
        Button
    </Button>
  ))
  .add('disabled', () => (
    <Button type="primary" isDisable>
        Button
    </Button>
  ))
  .add('link with target (without target turn on react-router)', () => (
    <Button href="/" target="_blank" type="primary">
        Button
    </Button>
  ))
  .add('rounded', () => (
    <Button type="primary" isRound>
        Button
    </Button>
  ))
  .add('with button group', () => (
    <>
        <ButtonGroup isMarginRight>
            <Button icon="edit">
                Button
            </Button>
            <Button icon="close" iconPlacement="right">
                Button
            </Button>
        </ButtonGroup>
        <ButtonGroup>
            <Button type="primary" icon="edit">
                Button
            </Button>
            <Button type="danger" icon="close" iconPlacement="right" isFill={false}>
                Button
            </Button>
        </ButtonGroup>
    </>
  ))
  .add('with click', () => (
    <Button type="primary" onClick={action('click')}>
        Button
    </Button>
  ))
