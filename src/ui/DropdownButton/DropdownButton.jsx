import React from 'react'
import PropTypes from 'prop-types'
import AntdDropdown from 'antd/es/dropdown'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const DropdownButton = ({
  className, style, htmlID, dataSet,

  overlay, isDisable, isVisible, trigger, onVisibleChange, onClick,
  children
}) => (
    <AntdDropdown.Button
        className={className}
        style={style}
        overlay={overlay}
        disabled={isDisable}
        onClick={onClick}
        trigger={trigger}
        {...safeObject({
          id: htmlID || null,
          visible: isVisible,
          onVisibleChange
        })}
        {...applyHtmlData(dataSet)}
    >
        {children}
    </AntdDropdown.Button>
)

DropdownButton.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  isDisable: PropTypes.bool,
  isVisible: PropTypes.bool,
  overlay: PropTypes.any,
  placement: PropTypes.oneOf(['bottomLeft', 'bottomCenter', 'bottomRight', 'topLeft', 'topCenter', 'topRight']),
  trigger: PropTypes.arrayOf(PropTypes.oneOf(['click', 'hover', 'contextMenu'])),
  onVisibleChange: PropTypes.func,
  onClick: PropTypes.func
}

DropdownButton.defaultProps = {
  isDisable: false,
  isVisible: null,
  placement: 'bottomLeft',
  trigger: ['click'],
  onVisibleChange: null
}

DropdownButton.displayName = 'DropdownButton'

export default DropdownButton
