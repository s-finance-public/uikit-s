import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import CheckBlock from './CheckBlock'

const stories = storiesOf('CheckBlock', module)

const options = [
  { value: 'one', label: '₽' },
  { value: 'two', label: 'Процент, %' },
  { value: 'three', label: 'мес.' }
]

stories
  .add('default', () => (
    <CheckBlock options={options} onChange={action('changed')}/>
  ))
  .add('with default value', () => (
    <CheckBlock options={options} defaultValue="three" onChange={action('changed')}/>
  ))
  .add('with fill', () => (
    <CheckBlock options={options} onChange={action('changed')} isFill/>
  ))
