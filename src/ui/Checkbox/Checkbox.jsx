import _ from 'lodash'
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import AntdChekbox from 'antd/es/checkbox'
import styled from 'styled-components'
import { colors } from 'styles'
import withFormProps from 'ui/common/withFormProps'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject, isSafe } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isChecked')
  return <AntdChekbox {...resolveProps}/>
})`
  user-select: none;
  transition: color .3s ease-in-out;
  
  &:hover {
    color: ${colors.primary6};
  }
  
  & .ant-checkbox-inner:after {
    margin-left: -1px;
  }
  
  & .ant-checkbox-checked::after {
    border-radius: 4px;
    border: 2px solid ${colors.primary};
  }
  
  & .ant-checkbox-inner {
    border-radius: 4px !important;
    background-color: ${props => (props.isChecked ? colors.primary : colors.gray3)};
    border-color: ${props => (props.isChecked ? colors.primary : colors.gray3)};
  }
  
  &:hover .ant-checkbox-inner {
    border-color: ${colors.primary} !important;
  }
  
  &:focus, &:active .ant-checkbox-inner {
    border-color: ${colors.primary} !important;
    box-shadow: 0 0 2px 4px ${colors.blue2};
  }
  
  * {
    user-select: none;
  }
`

const Checkbox = ({
  className, style, htmlID, dataSet,

  isDisable, children, value, tabIndex,
  ...props
}) => {
  const [isChecked, setIsChecked] = useState(false)

  const handleChange = () => {
    setIsChecked(!isChecked)
    if (props.onChange) props.onChange(!isChecked)
  }

  useEffect(() => {
    if (isSafe(_.get(props, 'isChecked', null))) setIsChecked(props.isChecked)
  }, [props.isChecked])

  return (
    <Root
      isChecked={isChecked}
      className={className}
      style={style}
      checked={isChecked}
      value={value}
      disabled={isDisable}
      onChange={handleChange}
      {...safeObject({
        tabIndex,
        id: htmlID || null
      })}
      {...applyHtmlData(dataSet)}
    >
      {children}
    </Root>
  )
}

Checkbox.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  isChecked: PropTypes.bool,
  value: PropTypes.any,
  isDisable: PropTypes.bool,
  label: PropTypes.string,
  tabIndex: PropTypes.number,

  onChange: PropTypes.func
}

Checkbox.defaultProps = {
  isChecked: false,
  isDisable: false,
  tabIndex: null
}

Checkbox.displayName = 'Checkbox'

const CheckboxGroup = withMargin({ displayName: 'CheckboxGroup', forceIsRem: false })(withFormProps({
  isOnlyShowForceValidate: true,
  displayName: 'CheckboxGroup'
})(AntdChekbox.Group))

CheckboxGroup.displayName = 'CheckboxGroup'

export { CheckboxGroup }

export default withMargin({ displayName: 'Checkbox' })(withFormProps({
  isOnlyShowForceValidate: true,
  displayName: 'Checkbox'
})(Checkbox))
