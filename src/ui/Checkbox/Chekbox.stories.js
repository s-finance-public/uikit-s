import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import Checkbox from './Checkbox'

const stories = storiesOf('Checkbox/Checkbox', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
    <>
      <Checkbox>Checkbox</Checkbox>
      <Checkbox>Checkbox</Checkbox>
    </>
  ))
  .add('with label', () => (
    <Checkbox>Checkbox</Checkbox>
  ))

storiesHelpers({
  stories,
  Component: props => <Checkbox {...props}>Checkbox</Checkbox>,
  isOnlyForceValidation: true,
  isWidth: false
})

stories
  .add('changed', () => (
    <Checkbox onChange={action('changed')}>Changed</Checkbox>
  ))
