import React from 'react'
import PropTypes from 'prop-types'
import List from 'antd/es/list'

const ListItem = ({
  className, style,

  title, description, extra, actions, children
}) => (
    <List.Item
        className={className}
        style={style}
        extra={extra}
        actions={actions}
    >
        {(!!title || !!description) &&
            <List.Item.Meta
                title={title}
                description={description}
            />
        }
        {children}
    </List.Item>
)

ListItem.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,

  title: PropTypes.any,
  description: PropTypes.any,
  extra: PropTypes.any,
  actions: PropTypes.array
}

ListItem.defaultProps = {
  title: null,
  description: null,
  extra: null,
  actions: []
}

export default ListItem
