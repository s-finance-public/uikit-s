import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Tag from './Tag'

const stories = storiesOf('Tag', module)

stories
  .add('default', () => (
    <Fragment>
      <Tag>Tag</Tag>
      <Tag>Tag</Tag>
    </Fragment>
  ))
  .add('closes', () => (
    <Fragment>
      <Tag onClose={action('close')}>Tag</Tag>
      <Tag onClose={action('close')}>Tag</Tag>
    </Fragment>
  ))
  .add('theme primary color', () => (
    <Fragment>
      <Tag color="primary">primary</Tag>
      <Tag color="primary">primary</Tag>
    </Fragment>
  ))
  .add('theme success color', () => (
    <Fragment>
      <Tag color="success">success</Tag>
      <Tag color="success">success</Tag>
    </Fragment>
  ))
  .add('theme danger color', () => (
    <Fragment>
      <Tag color="danger">danger</Tag>
      <Tag color="danger">danger</Tag>
    </Fragment>
  ))
  .add('theme warning color', () => (
    <Fragment>
      <Tag color="warning">warning</Tag>
      <Tag color="warning">warning</Tag>
    </Fragment>
  ))
  .add('custom color: #108ee9', () => (
    <Fragment>
      <Tag color="#108ee9">#108ee9</Tag>
      <Tag color="#108ee9">#108ee9</Tag>
    </Fragment>
  ))
  .add('color and textColor', () => (
    <Fragment>
        <Tag color="primary2" textColor="primary7" isMedium>Tag</Tag>
        <Tag color="gold2" textColor="gold7" isMedium>Tag</Tag>
        <Tag color="red2" textColor="red7" isMedium>Tag</Tag>
        <Tag color="green2" textColor="green8" isMedium>Tag</Tag>
        <Tag color="gray2" textColor="gray5" isMedium>Tag</Tag>
    </Fragment>
  ))
