import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import Spin from 'antd/es/spin'
import { colors } from 'styles'
import styled, { keyframes } from 'styled-components'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const animationFuncRootRotate = keyframes`
  0% {
    transform: translate(0%);
  }
  25% {
    transform: scale(0.5) rotate(90deg); 
  }
  50% {
    transform: translate(0%);
  }
  75% {
    transform: scale(0.5) rotate(275deg);
  }
`

const animationFuncRoot = keyframes`
  0% {
    transform: translate(0%);
  }
  25% {
    transform: scale(0.5); 
  }
  50% {
    transform: translate(0%);
  }
  75% {
    transform: scale(0.5);
  }
`

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isRotate')
  return <div {...resolveProps}/>
})`
  animation: ${props => (props.isRotate ? animationFuncRootRotate : animationFuncRoot)} 2s infinite ease-in-out;
  display: inline-block;

  & svg {
    max-width: 100%;
    max-height: 100%;
  }
`

const animationFunc = keyframes`
  0% {
    opacity: 0.4;
  }
  100% {
    opacity: 1;
  }
`

const One = styled.path`
  transition: opacity .3s ease-in-out;
  animation: ${animationFunc} 2s infinite ease-in-out;
  animation-delay: 1.5s;
`

const Two = styled.path`
  transition: opacity .3s ease-in-out;
  animation: ${animationFunc} 2s infinite ease-in-out;
  animation-delay: 1s;
`

const Three = styled.path`
  transition: opacity .3s ease-in-out;
  animation: ${animationFunc} 2s infinite ease-in-out;
  animation-delay: 0.5s;
`

const Four = styled.path`
  transition: opacity .3s ease-in-out;
  animation: ${animationFunc} 2s infinite ease-in-out;
`

const Loader = ({
  className, style, htmlID, dataSet,

  size, isColor, isRotate
}) => (
    <Root
        className={className}
        style={{
          ...style,
          width: `${size * 14}px`,
          height: `${size * 14}px`
        }}
        {...safeObject({
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
        isRotate={isRotate}
    >
      <svg viewBox="0 0 42 41">
        <g fill="none" fillRule="evenodd" transform="scale(0.64615385,0.640625)">
          <path fill="#D1E0FD" d="M32.5 64C14.71 64 0 49.523 0 32.015 0 14.477 14.678 0 32.469 0 50.259 0 65 14.477 65 32.015 65 49.523 50.29 64 32.5 64z"/>
          <path fill="#3EB5F1" d="M26.304 38.539L44.8 20.072a2.845 2.845 0 0 1 3.785-.193 2.433 2.433 0 0 1 .337 3.474c-.07.08-.07.08-.143.156L28.342 43.912a2.776 2.776 0 0 1-1.326.742 2.842 2.842 0 0 1-2.735-.727l-8.06-8.047c-.074-.076-.074-.076-.143-.155a2.433 2.433 0 0 1 .337-3.474 2.845 2.845 0 0 1 3.785.193l6.104 6.095z"/>
        </g>
      </svg>
    </Root>
)

Loader.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  isColor: PropTypes.bool,
  isRotate: PropTypes.bool,

  size: PropTypes.number
}

Loader.defaultProps = {
  size: 2,
  isColor: true,
  isRotate: false
}

const LoaderComponent = Loader

LoaderComponent.displayName = 'Loader'

Spin.setDefaultIndicator(<LoaderComponent/>)

export default LoaderComponent
