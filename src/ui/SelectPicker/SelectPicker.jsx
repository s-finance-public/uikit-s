import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import withMargin from 'ui/common/withMargin'
import { Context } from 'ui/Provider/Context'
import TouchSelect from 'ui/InputSelect/TouchSelect'
import Select from './Select'
import withFormProps from '../common/withFormProps'

const SelectPicker = ({
  className, style, htmlID, dataSet,

  value, options, placeholder, size, tabIndex, isAdaptive, isDisable,

  onChange, onFocus, onBlur,


  onChangeIsInput, onChangeIsFocus, ...props
}) => {
  const { responsiveModel } = useContext(Context)
  const [currentValue, setValue] = useState(value)

  useEffect(() => {
    if (value !== currentValue) {
      setValue(value)
    }
  }, [value])

  const handleChange = v => {
    setValue(v)

    if (onChange) onChange(v)

    if (onChangeIsInput) onChangeIsInput(true)
  }

  const handleFocus = () => {
    if (onFocus) onFocus()
    if (onChangeIsFocus) onChangeIsFocus(true)
  }

  const handleBlur = () => {
    if (onBlur) onBlur()
    if (onChangeIsInput) onChangeIsInput(true)
    if (onChangeIsFocus) onChangeIsFocus(false)
  }

  return (
    !(responsiveModel.isTouch && isAdaptive) ?
      <Select
          className={className}
          style={style}
          htmlID={htmlID}
          dataSet={dataSet}
          placeholder={placeholder}
          size={size}
          tabIndex={tabIndex}
          isDisable={isDisable}
          value={value}
          options={options}
          onFocus={onFocus}
          onBlur={onBlur}
          onChange={onChange}
          onChangeIsInput={onChangeIsInput}
          onChangeIsFocus={onChangeIsFocus}
          {...props}
      /> :
      <TouchSelect
          style={style}
          htmlID={htmlID}
          isDisable={isDisable}
          tabIndex={tabIndex}
          value={value}
          onChange={handleChange}
          options={options}
          onFocus={handleFocus}
          onBlur={handleBlur}
          size={size}
          placeholder={placeholder}
          {...props}
      />
  )
}

SelectPicker.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  placeholder: PropTypes.string,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  tabIndex: PropTypes.any,
  isAdaptive: PropTypes.bool,
  isDisable: PropTypes.bool,

  value: PropTypes.any,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.any,
    label: PropTypes.any,
    node: PropTypes.any
  })),
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,

  // withFormProps
  onChangeIsInput: PropTypes.func,
  onChangeIsFocus: PropTypes.func
}

SelectPicker.defaultProps = {
  value: null,
  placeholder: '',
  isDisable: false,
  size: 'lg',
  tabIndex: null,
  options: [],
  isAdaptive: true
}

SelectPicker.displayName = 'CommonSelectPicker'

export default withMargin({ displayName: 'CommonSelectPicker' })(SelectPicker)
