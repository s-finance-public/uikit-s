import _ from 'lodash'
import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import UiSelect from 'antd/es/select'
import withMargin from 'ui/common/withMargin'
import withFormProps from 'ui/common/withFormProps'
import Icon from 'ui/Icon'
import Block from 'ui/Block'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Select = ({
  className, style, htmlID, dataSet,

  value, options, placeholder, size, tabIndex, isDisable,

  onChange, onFocus, onBlur,


  onChangeIsInput, onChangeIsFocus
}) => {
  const [currentValue, setValue] = useState(value)

  useEffect(() => {
    if (value !== currentValue) {
      setValue(value)
    }
  }, [value])

  const handleChange = v => {
    setValue(v)

    if (onChange) onChange(v)

    if (onChangeIsInput) onChangeIsInput(true)
  }

  const handleFocus = () => {
    if (onFocus) onFocus()
    if (onChangeIsFocus) onChangeIsFocus(true)
  }

  const handleBlur = () => {
    if (onBlur) onBlur()
    if (onChangeIsInput) onChangeIsInput(true)
    if (onChangeIsFocus) onChangeIsFocus(false)
  }

  return (
    <UiSelect
      className={className}
      style={{ width: '100%', ...style }}
      {...safeObject({
        size: size === 'lg' ? 'large' : size === 'xs' ? 'small' : null,
        tabIndex,
        value: currentValue,
        id: htmlID || null
      })}
      {...applyHtmlData(dataSet)}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onChange={handleChange}
      disabled={isDisable}
      placeholder={placeholder}
      suffixIcon={
          <Icon
              iconType="redesign"
              icon="down"
              isMarginTop
              marginTop={size === 'lg' ? -0.3 : -0.2}
              color={!isDisable ? 'gray4' : 'gray3'}
              size={size === 'lg' ? 1.3 : size === 'sm' ? 1.2 : 1}
          />
      }
    >
      {_.map(options, (option, index) => (
        <UiSelect.Option
            key={option.value}
            value={option.value}
        >
          <Block htmlID={htmlID ? `${htmlID}-${index}` : null }>
            {option.node ?? option?.label}
          </Block>
        </UiSelect.Option>
      ))}
    </UiSelect>
  )
}

Select.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  placeholder: PropTypes.string,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  tabIndex: PropTypes.any,
  isDisable: PropTypes.bool,

  value: PropTypes.any,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.any,
    label: PropTypes.any
  })),
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,

  // withFormProps
  onChangeIsInput: PropTypes.func,
  onChangeIsFocus: PropTypes.func
}

Select.defaultProps = {
  value: null,
  placeholder: '',
  isDisable: false,
  size: 'lg',
  tabIndex: null,
  options: []
}

Select.displayName = 'SelectPicker'

export default withMargin({ displayName: 'SelectPicker' })(withFormProps({ displayName: 'SelectPicker' })(Select))
