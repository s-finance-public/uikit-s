import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import { moment } from 'utils/moment'
import Button from 'ui/Button'
import Icon from 'ui/Icon'
import DatePicker from './DatePicker'

const stories = storiesOf('DatePicker', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
      {story()}
    </div>
))

stories
  .add('default', () => (
    <DatePicker />
  ))
  .add('with clear', () => (
    <DatePicker isAllowClear/>
  ))
  .add('month picker', () => (
    <DatePicker mode="month"/>
  ))
  .add('month picker reversed', () => (
    <DatePicker mode="month" isReverseYear/>
  ))
  .add('with placeholder', () => (
    <DatePicker placeholder="Select birthday" value={null}/>
  ))
  .add('disabled', () => (
    <DatePicker label="Birthday" isDisable/>
  ))
  .add('sizes', () => (
    <Fragment>
        <DatePicker label="Birthday" size="xs" isMarginBottom/>
        <DatePicker label="Birthday" size="sm" isMarginBottom/>
        <DatePicker label="Birthday" size="lg" isMarginBottom/>
        <DatePicker label="Birthday" isVerticalLabel={false} size="xs" isMarginBottom/>
        <DatePicker label="Birthday" isVerticalLabel={false} size="sm" isMarginBottom/>
        <DatePicker label="Birthday" isVerticalLabel={false} size="lg"/>
    </Fragment>
  ))

storiesHelpers({
  stories,
  Component: DatePicker
})

stories
  .add('with min & max', () => (
    <DatePicker
        value={moment('18.09.1992', 'DD.MM.YYYY').toDate()}
        min={moment('01.09.1992', 'DD.MM.YYYY').toDate()}
        max={moment('01.09.1993', 'DD.MM.YYYY').toDate()}
    />
  ))
  .add('with left & right & prefix & postfix', () => (
    <DatePicker
        label="Birthday"
        value={moment('18.09.1992', 'DD.MM.YYYY').toDate()}
        min={moment('01.09.1992', 'DD.MM.YYYY').toDate()}
        max={moment('01.09.1993', 'DD.MM.YYYY').toDate()}
        prefix="$"
        postfix={<Button icon="search" type="primary" isMarginRight marginRight={-12}/>}
        left="Sum"
        right={<Icon icon="info-circle"/>}
    />
  ))
  .add('changed', () => (
    <DatePicker onChange={action('date value')}/>
  ))
