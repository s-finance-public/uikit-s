import _ from 'lodash'
import fp from 'lodash/fp'
import React, { useState, useEffect, useContext } from 'react'
import PropTypes from 'prop-types'
import Block from 'ui/Block'
import Calendar from 'antd/es/calendar'
import { moment, MONTHES } from 'utils/moment'
import withMargin from 'ui/common/withMargin'
import Input from 'ui/Input'
import Popover from 'ui/Popover'
import Icon from 'ui/Icon'
import styled from 'styled-components'
import SelectPicker from 'ui/SelectPicker'
import { Row, Col } from 'ui/Grid'
import { Context } from 'ui/Provider/Context'
import { safeObject, isSafe } from 'utils/object'

const RootCalendar = styled.div`
  max-width: 420px;
`

const RootInputComponent = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isTouch')
  return <Input {...resolveProps}/>
})`
  ${props => (props.isTouch ? `
    & input {
      line-height: inherit;
    }` : '')
}
`

const isConstraint = (value, min, max) => {
  const isValidMin = min ? value >= min : true
  const isValidMax = max ? value <= max : true
  return value && isValidMin && isValidMax
}

const DatePicker = ({
  className, style, htmlID, dataSet,

  size, mode, isDisable, min, max, isAllowClear, placeholder, tabIndex,
  left, right, prefix, postfix, isReverseYear, yearConstraint,

  onChange, onFocus, onBlur,
  ...props
}) => {
  const { responsiveModel } = useContext(Context)

  const dateFormat = mode === 'month' ? 'MM.YYYY' : 'DD.MM.YYYY'

  const [value, setValue] = useState(null)
  const [isInput, setIsInput] = useState(false)
  const [isFocus, setIsFocus] = useState(false)
  const [isFocusInput, setIsFocusInput] = useState(false)
  const [isOverCalendar, setIsOverCalendar] = useState(false)

  const handleChange = (isChangeOutside = true) => v => {
    let currentValue = null
    if (!v && (!value || /_*/ig.test(value))) {
      currentValue = moment(new Date(), dateFormat).format(dateFormat)
    }

    if (v) {
      if (!responsiveModel.isTouch || mode === 'month') {
        currentValue = _.isString(v) ? v : moment(v, dateFormat).format(dateFormat)
      } else {
        currentValue = moment(v, 'YYYY-MM-DD').format(dateFormat)
      }
    }
    setValue(currentValue)

    if (isChangeOutside) {
      setIsInput(true)
    }

    if (!_.isString(v)) {
      setIsOverCalendar(false)
    }

    const resolveValue = !/_/ig.test(currentValue) && moment(currentValue, dateFormat).isValid() ? moment(currentValue, dateFormat) : null
    if (onChange && resolveValue && isConstraint(resolveValue.toDate(), min, max)) {
      onChange(moment(currentValue, dateFormat).toDate())
    }
  }

  useEffect(() => {
    if (!isFocus) {
      const propsValue = !props.value ? null : moment(props.value, dateFormat).format(dateFormat)
      if (!propsValue && !!value) {
        setValue('')
      } else if (!!propsValue && propsValue !== value) {
        setValue(moment(props.value, dateFormat).format(dateFormat))
      }
    }
  }, [
    !props.value ? null : moment(props.value, dateFormat).format(dateFormat)
  ])

  const handleFocus = () => {
    setIsFocus(true)
    setIsFocusInput(true)
    if (onFocus) onFocus()
  }

  const handleBlur = (isForce = false) => () => {
    setIsFocusInput(false)
    if (!isOverCalendar || isForce) {
      if (/_/ig.test(value) || !isConstraint(moment(value, dateFormat).toDate(), min, max) ||
        !moment(value, dateFormat).isValid()) {
        setValue('')
        if (onChange) onChange(null)
      }

      setIsFocus(false)
      if (onBlur) onBlur()
    }
  }

  const currentYear = new Date().getFullYear()
  const currentValue = isSafe(value) && !/_/ig.test(value) ? moment(value, dateFormat).toDate() : new Date()

  const handleSetYear = year => {
    const v = new Date(currentValue)
    v.setFullYear(Number(year))
    handleChange()(v)
  }

  const handleSetMonth = month => {
    const v = new Date(currentValue)
    v.setMonth(Number(month))
    handleChange()(v)
  }

  const handleOverCalendar = () => setIsOverCalendar(true)
  const handleLeaveCalendar = () => {
    setIsOverCalendar(false)
    if (!isFocusInput) handleBlur(true)()
  }

  const yearRange = isReverseYear ? _.rangeRight(currentYear - 80, currentYear + yearConstraint) : _.range(currentYear - 80, currentYear + yearConstraint)

  const CalendarIcon = (
      <Block isMarginTop marginTop={size !== 'xs' ? 3 : 4} isRem={false}>
        <Icon
            icon="calendar" iconType="redesign"
            size={size === 'lg' ? 16 : 14}
            isRem={false}
            color="primary"
        />
      </Block>
  )

  const InputComponent = (
      <RootInputComponent
          isTouch={responsiveModel.isTouch}
          className={className}
          style={{
            width: '100%',
            ...style
          }}
          size={size}
          onMetaFocus={handleFocus}
          onMetaBlur={handleBlur()}
          onChange={handleChange()}
          placeholder={placeholder}
          isDisable={isDisable}
          isAllowClear={isAllowClear && !isDisable}
          value={!responsiveModel.isTouch || mode === 'month' ? value : moment(value, dateFormat).format('YYYY-MM-DD')}
          onPressEnter={isFocus ? handleBlur() : handleFocus}
          onDown={handleFocus}
          onFocus={handleFocus}
          left={left}
          right={right}
          prefix={prefix}
          postfix={CalendarIcon}
          htmlID={htmlID}
          internalUnmask={v => v.replace(/._/gi, '')}
          dataSet={dataSet}
          tabIndex={tabIndex || 0}
          {...safeObject({
            mask: !responsiveModel.isTouch || mode === 'month' ? mode === 'month' ? '99.9999' : '99.99.9999' : null,
            type: responsiveModel.isTouch && mode === 'date' ? 'date' : null
          })}
          label={props.label}
          error={props.error}
          warn={props.warn}
          help={props.help}
          isFullWidth={props.isFullWidth}
          isShowForceValidate={props.isShowForceValidate}
          errorWidth={props.errorWidth}
      />
  )

  return !responsiveModel.isTouch ?
      <Popover
          content={
            <RootCalendar onMouseMove={handleOverCalendar} onMouseLeave={handleLeaveCalendar}>
              <Calendar
                  fullscreen={false}
                  mode={mode === 'month' ? 'year' : 'month'}
                  headerRender={() => (
                      <Row isFlex justify="space-between" isMarginBottom marginBottom={10} isRem={false}>
                        <Col style={{ width: '140px' }}>
                          <SelectPicker
                              size="xs"
                              placeholder="Год"
                              options={fp.map(year => ({
                                value: `${year}`,
                                label: `${year}`
                              }))(yearRange)}
                              onChange={handleSetYear}
                              {...safeObject({
                                value: currentValue.getFullYear() ? `${currentValue.getFullYear()}` : null
                              })}
                          />
                        </Col>
                        {mode !== 'month' &&
                        <Col style={{ width: '112px' }} isMarginLeft marginLeft={0.5}>
                          <SelectPicker
                              size="xs"
                              placeholder="Месяц"
                              options={fp.map(month => ({
                                value: `${month.id}`,
                                label: month.name
                              }))(MONTHES)}
                              onChange={handleSetMonth}
                              {...safeObject({
                                value: currentValue.getMonth() ? `${currentValue.getMonth()}` : null
                              })}
                          />
                        </Col>
                        }
                      </Row>
                  )}
                  onChange={handleChange()}
                  defaultValue={(() => {
                    const defaultDate = moment(new Date(), dateFormat)
                    if (max) return moment(max, dateFormat)
                    if (min) return moment(min, dateFormat)
                    return defaultDate
                  })()}
                  {...safeObject({
                    disabledDate: min || max ? currentDate => {
                      let isResolve = false
                      if (min) {
                        isResolve = currentDate < moment(min, dateFormat)
                      }
                      if (max && !isResolve) {
                        isResolve = currentDate > moment(max, dateFormat)
                      }
                      return isResolve
                    } : null,
                    value: moment(value, dateFormat).isValid() ? moment(value, dateFormat) : null
                  })}
              />
            </RootCalendar>
          }
          isVisible={isFocus}
          placement="bottomLeft"
      >
        {InputComponent}
      </Popover> : InputComponent
}

DatePicker.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  value: PropTypes.object,
  left: PropTypes.any,
  right: PropTypes.any,
  prefix: PropTypes.any,
  postfix: PropTypes.any,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  mode: PropTypes.oneOf(['date', 'month']),
  placeholder: PropTypes.string,
  isDisable: PropTypes.bool,
  isAllowClear: PropTypes.bool,
  min: PropTypes.object,
  max: PropTypes.object,
  tabIndex: PropTypes.number,
  isReverseYear: PropTypes.bool,
  yearConstraint: PropTypes.number,

  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func
}

DatePicker.defaultProps = {
  value: null,
  left: null,
  right: null,
  prefix: null,
  postfix: null,
  size: 'lg',
  mode: 'date',
  placeholder: '',
  isDisable: false,
  min: null,
  max: null,
  isAllowClear: false,
  tabIndex: null,
  isReverseYear: false,
  yearConstraint: 1
}

DatePicker.displayName = 'DatePicker'

export default withMargin({ displayName: 'DatePicker' })(
  DatePicker
)
