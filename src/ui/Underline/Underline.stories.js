import React from 'react'
import { storiesOf } from '@storybook/react'
import Underline from './Underline'

const stories = storiesOf('Underline', module)

stories
  .add('default', () => (
    <Underline>
        Underline
    </Underline>
  ))
  .add('with color', () => (
    <Underline color="success">
        Underline
    </Underline>
  ))
