import fp from 'lodash/fp'
import React from 'react'
import PropTypes from 'prop-types'
import withMargin from 'ui/common/withMargin'
import AntdDescription from 'antd/es/descriptions'
import { safeObject } from 'utils/object'
import styled from 'styled-components'
import { colors } from 'styles'

const Title = styled.div`
  color: ${props => colors[props.color]};
`

const Description = ({
  options,
  title,
  layout,
  isColon,
  column,
  isBordered,
  size,
  htmlID,
  color
}) => (
  <AntdDescription
    bordered={isBordered}
    colon={isColon}
    column={column}
    layout={layout}
    {...safeObject({
      size: size === 'lg' ? null : size === 'sm' ? 'middle' : 'small',
      title: title ? <Title color={color}>{title}</Title> : null
    })}
  >
    {fp.map(option => (
      <AntdDescription.Item
        key={option.id}
        label={option.label}
        {...safeObject({
          id: htmlID ? `${htmlID}-${option.id}` : null,
          span: option.span
        })}
      >
        {option.node}
      </AntdDescription.Item>
    ))(options)}
  </AntdDescription>
)

Description.propTypes = {
  title: PropTypes.string,
  layout: PropTypes.oneOf(['horizontal', 'vertical']),
  isColon: PropTypes.bool,
  htmlID: PropTypes.string,
  column: PropTypes.number,
  isBordered: PropTypes.bool,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  options: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    node: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    span: PropTypes.number,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node, PropTypes.object]).isRequired
  })),
  color: PropTypes.oneOf(['primary', 'info', 'success', 'danger', 'warning', 'default'])
}

Description.defaultProps = {
  isColon: true,
  layout: 'horizontal',
  size: 'lg',
  column: 3,
  isBordered: false,
  color: 'primary'
}

Description.displayName = 'Description'

export default withMargin({ displayName: 'Description' })(Description)
