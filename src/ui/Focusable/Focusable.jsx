import _ from 'lodash'
import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { colors } from 'styles'
import withMargin from 'ui/common/withMargin'
import { safeObject } from 'utils/object'
import { applyHtmlData } from 'utils/htmlData'
import { Context } from 'ui/Provider/Context'
import Block from 'ui/Block'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'color')
  _.unset(resolveProps, 'isFocus')
  _.unset(resolveProps, 'isDisable')
  return <div {...resolveProps}/>
})`
  outline: none;
  cursor: pointer;
  user-select: none;
  transition: none;
  
  & * {
    transition: none;
  }
`

const Focusable = ({
  className, style, htmlID, dataSet,
  color, children, tabIndex, blockType,
  isDisable, isRem, padding, borderRadius,
  onFocus, onBlur, onClick
}) => {
  const { remPoint } = useContext(Context)
  const [isFocus, setIsFocus] = useState(false)

  const handleFocus = () => {
    if (isDisable) return
    setIsFocus(true)
    if (onFocus) onFocus()
  }

  const handleBlur = () => {
    if (isDisable) return
    setIsFocus(false)
    if (onBlur) onBlur(false)
  }

  const handleKeyPress = e => {
    if (isDisable) return
    if (e?.charCode === 13 && onClick) onClick()
  }

  return (
    <Root
        className={className}
        cursor="pointer"
        isDisable={isDisable}
        color={color}
        onFocus={handleFocus}
        onBlur={handleBlur}
        onMouseOver={handleFocus}
        onMouseLeave={handleBlur}
        onKeyPress={handleKeyPress}
        style={safeObject({
          ...style,
          display: blockType,
          padding: _.isNumber(padding) ? `${isRem ? remPoint * padding : padding}px` : padding,
          borderRadius: _.isNumber(borderRadius) ? `${isRem ? remPoint * borderRadius : borderRadius}px` : borderRadius,
          background: isFocus && !isDisable ?
            colors[color] ? colors[color] : color : 'transparent',
          cursor: isDisable ? 'not-allowed' : null
        })}
        {...safeObject({
          id: htmlID || null,
          tabIndex: !isDisable ? tabIndex : null,
          onClick: !isDisable ? onClick : null
        })}
        {...applyHtmlData(dataSet)}
    >
        <Block isCenter>
            {children}
        </Block>
    </Root>
  )
}

Focusable.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  color: PropTypes.string, // colors of theme or custom color
  tabIndex: PropTypes.number,
  blockType: PropTypes.oneOf(['inline-block', 'block', 'flex', 'inline-flex']),
  isDisable: PropTypes.bool,
  isRem: PropTypes.bool,
  padding: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  borderRadius: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

  onClick: PropTypes.func,

  onFocus: PropTypes.func,
  onBlur: PropTypes.func
}

Focusable.defaultProps = {
  color: 'primary1',
  tabIndex: 1,
  blockType: 'inline-block',
  isDisable: false,
  padding: '4px',
  borderRadius: '4px',
  isRem: true
}

Focusable.displayName = 'Focusable'

export default withMargin({ displayName: 'Focusable' })(Focusable)
