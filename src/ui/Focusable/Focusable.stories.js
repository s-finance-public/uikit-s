import React, { useState } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Icon from 'ui/Icon'
import Block from 'ui/Block'
import Text from 'ui/Typography/Text'
import Input from 'ui/Input'
import Popover from 'ui/Popover'
import Focusable from './Focusable'

const stories = storiesOf('Focusable', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
    <>
        <Block isInline isMarginRight>
            <Input tabIndex={1}/>
        </Block>
        <Focusable isMarginRight onClick={action('clicked')} tabIndex={1}>
            <Text>One</Text>
        </Focusable>
        <Focusable isMarginRight onClick={action('clicked')} tabIndex={1}>
            <Icon icon="edit"/>
        </Focusable>
        <Focusable isMarginRight onClick={action('clicked')} tabIndex={1}>
            <Text>
                <Text isMarginRight marginRight={0.5}>Down</Text>
                <Icon icon="chevron-down" iconType="redesign"/>
            </Text>
        </Focusable>
    </>
  ))
  .add('active & disabled', () => (
    <>
        <Block isInline isMarginRight>
            <Input tabIndex={1}/>
        </Block>
        <Focusable isMarginRight onClick={action('clicked')} tabIndex={1}>
            <Text>One</Text>
        </Focusable>
        <Focusable isMarginRight isDisable onClick={action('clicked')} tabIndex={1}>
            <Icon icon="edit"/>
        </Focusable>
    </>
  ))
  .add('custom color', () => (
    <>
        <Block isInline isMarginRight>
            <Input tabIndex={1}/>
        </Block>
        <Focusable isMarginRight onClick={action('clicked')} tabIndex={1}>
            <Text>One</Text>
        </Focusable>
        <Focusable isMarginRight color="red2" onClick={action('clicked')} tabIndex={1}>
            <Icon icon="edit"/>
        </Focusable>
        <Focusable isMarginRight color="green2" onClick={action('clicked')} tabIndex={1}>
            <Text>
                <Text isMarginRight marginRight={0.5}>Down</Text>
                <Icon icon="chevron-down" iconType="redesign"/>
            </Text>
        </Focusable>
    </>
  ))
  .add('focusable prefix in Input', () => {
    const [isFocus, setIsFocus] = useState(false)
    return (
        <Input
          tabIndex={1}
          postfix={(
              <Focusable
                  onFocus={() => setIsFocus(true)}
                  onBlur={() => setIsFocus(false)}
                  tabIndex={1}
                  padding={0.5}
                  onClick={action('clicked')}
              >
                  <Icon icon="edit" color={!isFocus ? 'text' : 'primary'}/>
              </Focusable>
          )}
        />
    )
  })
  .add('example with popover', () => (
    <>
        <Block isInline isMarginRight>
            <Input tabIndex={1}/>
        </Block>
        <Popover
            content={<Block><Text>Any</Text></Block>}
        >
            <Focusable color="red2" onClick={action('clicked')} tabIndex={1}>
                <Icon icon="edit"/>
            </Focusable>
        </Popover>
    </>
  ))
