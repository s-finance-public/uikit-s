import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Slider from './Slider'

const stories = storiesOf('Slider', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
    {story()}
  </div>
))

stories
  .add('default', () => (
    <Slider onChange={action('changed')}/>
  ))
  .add('with min & max', () => (
    <Slider onChange={action('changed')} min={2} max={10}/>
  ))
  .add('disabled', () => (
    <Slider onChange={action('changed')} min={2} max={10} isDisable/>
  ))
  .add('with normalizer', () => (
    <Slider
      onChange={action('changed')}
      min={2}
      max={10}
      sliderNormalizer={v => `${v} %`}
    />
  ))
  .add('with step', () => (
    <Slider onChange={action('changed')} step={10}/>
  ))
