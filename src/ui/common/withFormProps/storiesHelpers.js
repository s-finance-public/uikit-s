import React, { Fragment } from 'react'

export default ({
  stories,
  Component,
  isOnlyForceValidation = false,
  isWidth = true,
  namePrefix = '',
  customProps
}) => {
  const currentPrefix = `form${namePrefix ? ` ${namePrefix}:` : ':'}`

  if (isWidth) {
    stories
      .add(`${currentPrefix}custom width`, () => (
          <Fragment>
            <Component isFullWidth={false} width="70%" isMarginBottom {...customProps}/>
            <Component isFullWidth={false} width="80%" {...customProps}/>
          </Fragment>
      ))
  }

  stories
    .add(`${currentPrefix}with form label`, () => (
        <Component label="Label" {...customProps}/>
    ))

  if (!isOnlyForceValidation) {
    stories
      .add(`${currentPrefix}with error only when change`, () => (
        <Component label="Count" error="Any error" {...customProps}/>
      ))
      .add(`${currentPrefix}with warning only when change`, () => (
        <Component label="Count" warn="Any warning" {...customProps}/>
      ))
  }

  stories
    .add(`${currentPrefix}with force error`, () => (
      <Component label="Count" error="Any error" isShowForceValidate {...customProps}/>
    ))
    .add(`${currentPrefix}with force warning`, () => (
      <Component label="Count" warn="Any warning" isShowForceValidate {...customProps}/>
    ))
    .add(`${currentPrefix}with help`, () => (
      <Component label="Count" help="Any help" {...customProps}/>
    ))
    .add(`${currentPrefix}multi line order demo `, () => (
        <>
          <Component label="Error" error="Any error" {...customProps}/>
          <Component label="Warning" warn="Any warning" {...customProps}/>
          <Component label="Help" help="Any help" {...customProps}/>
          <Component label="Error" error="Any error" size="sm" {...customProps}/>
          <Component label="Warning" warn="Any warning" size="sm" {...customProps}/>
          <Component label="Help" help="Any help" size="sm" {...customProps}/>
          <Component label="Error" error="Any error" size="xs" {...customProps}/>
          <Component label="Warning" warn="Any warning" size="xs" {...customProps}/>
          <Component label="Help" help="Any help" size="xs" {...customProps}/>
        </>
    ))
}
