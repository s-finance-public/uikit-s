import React from 'react'
import { storiesOf } from '@storybook/react'
import Input from 'ui/Input'
import InputSelect from 'ui/InputSelect'
import DatePicker from 'ui/DatePicker'
import SelectPicker from 'ui/SelectPicker'

const stories = storiesOf('Input Controls', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

const options = [
  { value: 'one', label: 'One' },
  { value: 'two', label: 'Two' },
  { value: 'three', label: 'Three' },
  { value: 'four', label: 'Four' },
  { value: 'five', label: 'Five' },
  { value: 'six', label: 'Six' },
  { value: 'seven', label: 'Seven' }
]

stories
  .add('vertical margin orders', () => (
        <>
            <Input label="Input One" error="error" isShowForceValidate/>
            <Input label="Input Two" marginTop={1.5} error="error" isShowForceValidate/>
            <InputSelect label="InputSelect One" options={options} error="error" isShowForceValidate/>
            <InputSelect label="InputSelect Two" options={options} marginTop={1.5} error="error" isShowForceValidate/>
            <SelectPicker label="SelectPicker One" options={options} error="error" isShowForceValidate/>
            <SelectPicker label="SelectPicker Two" options={options} marginTop={1.5} error="error" isShowForceValidate/>
            <DatePicker label="DatePicker One" options={options} error="error" isShowForceValidate/>
            <DatePicker label="DatePicker Two" options={options} marginTop={1.5} error="error" isShowForceValidate/>
        </>
  ))
