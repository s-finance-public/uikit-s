import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import Alert from './Alert'

const stories = storiesOf('Alert', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
    <Alert
        message="Any message"
    />
  ))
  .add('without icon', () => (
    <Alert
        message="Any message"
        isShowIcon={false}
    />
  ))
  .add('types', () => (
    <Fragment>
        <Alert
            type="success"
            message="Any message"
            isMarginBottom
        />
        <Alert
            type="info"
            message="Any message"
            isMarginBottom
        />
        <Alert
            type="error"
            message="Any message"
            isMarginBottom
        />
        <Alert
            type="warning"
            message="Any message"
        />
    </Fragment>
  ))
  .add('closable', () => (
    <Fragment>
        <Alert
            type="success"
            message="Any message"
            isClosable
            isMarginBottom
        />
        <Alert
            type="info"
            message="Any message"
            isClosable
            isMarginBottom
        />
        <Alert
            type="error"
            message="Any message"
            isClosable
            isMarginBottom
        />
        <Alert
            type="warning"
            message="Any message"
            isClosable
        />
    </Fragment>
  ))
