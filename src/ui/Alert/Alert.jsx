import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import AntAlert from 'antd/es/alert'
import styled from 'styled-components'
import { colors } from 'styles'
import { hexToRgba } from 'utils/colors'
import withMargin from 'ui/common/withMargin'
import Icon from 'ui/Icon'
import Block from 'ui/Block'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'closeColor')
  return <AntAlert {...resolveProps}/>
})`
  display: flex;
  width: 100% !important;
  padding: 12px 16px 8px 16px !important;
  
  & .ant-alert-close-icon {
    font-size: 12px;
    color: ${props => props.closeColor} !important;
    
    * {
      color: ${props => props.closeColor} !important;
    }
  }
  
  & .ant-alert-close-icon:hover {
    color: ${props => props.closeColor} !important;
  }
  
`

const IconContainer = styled(Block)`
  position: initial !important;
  margin-right: 14px !important;
  margin-top: -11.2px; 
`

const Alert = ({
  className, style, htmlID, dataSet,

  type, message, isShowIcon, icon, isClosable, onClick
}) => {
  let color = 'text'
  let iconDefault = ''
  const iconType = 'design'
  switch (type) {
    case 'success':
      iconDefault = 'check-circle'
      color = colors.green4
      break
    case 'info':
      iconDefault = 'info-circle'
      color = colors.primary
      break
    case 'warning':
      iconDefault = 'warning'
      color = colors.warning
      break
    case 'error':
      iconDefault = 'warning'
      color = colors.errorLight
      break
  }

  const resolveIcon = icon ? <IconContainer>{icon}</IconContainer> : iconDefault ? (
    <IconContainer>
      <Icon iconType={iconType} icon={iconDefault} color={color} size={1.2}/>
    </IconContainer>
  ) : null

  let closeColor = ''
  switch (type) {
    case 'success':
      closeColor = colors.green6
      break
    case 'info':
      closeColor = colors.blue6
      break
    case 'warning':
      closeColor = colors.gold6
      break
    case 'error':
      closeColor = colors.red6
      break
  }

  return (
    <Root
      className={className}
      style={style}
      message={null}
      description={<Block paddingRight={1.5} style={{ flex: 1 }}>{message}</Block>}
      type={type}
      showIcon={resolveIcon && isShowIcon}
      closable={isClosable}
      onClick={onClick}
      closeColor={hexToRgba(closeColor, 0.5)}
      {...safeObject({
        icon: resolveIcon,
        id: htmlID || null
      })}
      {...applyHtmlData(dataSet)}
    />
  )
}

Alert.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,
  onClick: PropTypes.func,

  type: PropTypes.oneOf(['success', 'info', 'warning', 'error']),
  message: PropTypes.any,
  isShowIcon: PropTypes.bool,
  icon: PropTypes.any,
  isClosable: PropTypes.bool
}

Alert.defaultProps = {
  type: 'warning',
  isShowIcon: true,
  icon: null,
  isClosable: false
}

Alert.displayName = 'Alert'

export default withMargin({ displayName: 'Alert' })(Alert)
