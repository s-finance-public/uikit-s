import React from 'react'
import { storiesOf } from '@storybook/react'
import Badge from 'ui/Badge'

const stories = storiesOf('Badge', module)

stories
  .add('default', () => (
    <Badge label={100}/>
  ))
  .add('custom background', () => (
    <Badge label={100} background="success"/>
  ))
  .add('custom color', () => (
    <Badge label={100} color="gray1" background="danger"/>
  ))
  .add('custom background & color', () => (
    <Badge label={100} background="#eee" color="danger"/>
  ))
  .add('with status', () => (
    <>
      <Badge status="success" statusText="Success" isMarginRight/>
      <Badge status="processing" statusText="Processing" isMarginRight/>
      <Badge status="error" statusText="Error" isMarginRight/>
      <Badge status="warning" statusText="Warning"/>
    </>
  ))
