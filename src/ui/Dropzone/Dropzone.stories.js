import React from 'react'
import { storiesOf } from '@storybook/react'
import Button from 'ui/Button'
import { notifySuccess, notifyError } from 'utils/notification'
import Dropzone from './Dropzone'

const stories = storiesOf('Dropzone', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
    {story()}
  </div>
))

const handleDrop = (acceptedFiles, rejectedFiles) => {
  if (acceptedFiles) {
    notifySuccess('Accepted files:', acceptedFiles.map(f => f.name))
  }
  if (rejectedFiles) {
    notifyError('Rejected files:', rejectedFiles.map(f => `Filename: ${f.file.name}, reason: ${f.errors.map(e => e.message)}`))
  }
}

const renderComponent = ({ isDisable }) => (<Button isDisable={isDisable}>Drop some files</Button>)

const renderComponentWithActiveHandle = ({ isDragActive, isDragAccept, isDragReject }) => {
  let component
  if (isDragAccept) {
    component = (<Button isDisable>accepted</Button>)
  } else if (isDragReject) {
    component = (<Button isDisable>rejected</Button>)
  } else if (!isDragActive) {
    component = (<Button>Drop some files</Button>)
  }

  return component
}

stories
  .add('default', () => (
    <Dropzone onDrop={handleDrop} renderComponent={renderComponent} />
  ))
  .add('render with state handle', () => (
    <Dropzone onDrop={handleDrop} renderComponent={renderComponentWithActiveHandle} accept="application/pdf" />
  ))
  .add('multiple', () => (
    <Dropzone onDrop={handleDrop} renderComponent={renderComponent} multiple />
  ))
  .add('accept', () => (
    <Dropzone onDrop={handleDrop} renderComponent={renderComponent} accept="application/pdf" />
  ))
  .add('isDisable', () => (
    <Dropzone onDrop={handleDrop} renderComponent={renderComponent} isDisable />
  ))
  .add('only accepted', () => (
    <Dropzone onDropAccepted={handleDrop} renderComponent={renderComponent} accept="application/pdf" />
  ))
