import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import Divider from 'ui/Typography/Divider'
import Title from './Title'

const stories = storiesOf('Typography/Title', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

const text = 'Title'

stories
  .add('default level 1', () => (
    <Fragment>
      <Title>{text}</Title>
      <Title>{text}</Title>
    </Fragment>
  ))
  .add('level 2', () => (
    <Fragment>
        <Title level={2}>{text}</Title>
        <Title level={2}>{text}</Title>
    </Fragment>
  ))
  .add('level 3', () => (
    <Fragment>
        <Title level={3}>{text}</Title>
        <Title level={3}>{text}</Title>
    </Fragment>
  ))
  .add('level 4', () => (
    <Fragment>
        <Title level={4}>{text}</Title>
        <Title level={4}>{text}</Title>
    </Fragment>
  ))
  .add('secondary', () => (
    <Fragment>
      <Title type="secondary">{text}</Title>
      <Title type="secondary">{text}</Title>
    </Fragment>
  ))
  .add('primary', () => (
    <Fragment>
        <Title type="primary">{text}</Title>
        <Title type="primary">{text}</Title>
    </Fragment>
  ))
  .add('success', () => (
    <Fragment>
      <Title type="success">{text}</Title>
      <Title type="success">{text}</Title>
    </Fragment>
  ))
  .add('danger', () => (
    <Fragment>
      <Title type="danger">{text}</Title>
      <Title type="danger">{text}</Title>
    </Fragment>
  ))
  .add('danger', () => (
    <Fragment>
      <Title type="danger">{text}</Title>
      <Title type="danger">{text}</Title>
    </Fragment>
  ))
  .add('warning', () => (
    <Fragment>
      <Title type="warning">{text}</Title>
      <Title type="warning">{text}</Title>
    </Fragment>
  ))
  .add('mark', () => (
    <Fragment>
      <Title isMark>{text}</Title>
      <Title isMark>{text}</Title>
    </Fragment>
  ))
  .add('deleted', () => (
    <Fragment>
      <Title isDelete>{text}</Title>
      <Title isDelete>{text}</Title>
    </Fragment>
  ))
  .add('underline', () => (
    <Fragment>
      <Title isUnderline>{text}</Title>
      <Title isUnderline>{text}</Title>
    </Fragment>
  ))
  .add('strong and medium', () => (
    <Fragment>
      <Title isStrong>{text}</Title>
      <Divider/>
      <Title isMedium>{text}</Title>
    </Fragment>
  ))
  .add('ellipsis', () => (
    <Fragment>
      <Title isEllipsis>
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText
      </Title>
      <Title isEllipsis>
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText
      </Title>
    </Fragment>
  ))
  .add('align', () => (
    <Fragment>
        <Title>{text}</Title>
        <Title align="center">{text}</Title>
        <Title align="right">{text}</Title>
    </Fragment>
  ))
