import _ from 'lodash'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Typography from 'antd/es/typography'
import { colors } from 'styles'
import withMargin from 'ui/common/withMargin'
import { Context } from 'ui/Provider/Context'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isStrong')
  _.unset(resolveProps, 'isMedium')
  _.unset(resolveProps, 'isRem')
  return <Typography.Title {...resolveProps}/>
})`
  text-align: ${props => props.align};
  font-weight: ${props => (props.isStrong ? 600 : props.isMedium ? 500 : 'inherit')};
  transition: color .3s ease-in-out;
  cursor: ${props => (props.onClick ? 'pointer' : 'inherit')};

  & del {
    background: inherit;
  }
  
  & code {
    color: inherit;
  }
  
  & a, a:hover, a:visited, a:focus {
    color: inherit;
    text-decoration: inherit;
    cursor: pointer;
  }
`

const Title = ({
  className, style, htmlID, dataSet,

  onClick, level, type, isCode, isDisable,
  isMark, isDelete, isUnderline, isStrong, isMedium, isEllipsis, align,
  size, isRem,
  children
}) => {
  const { remPoint } = useContext(Context)

  return (
      <Root
          className={className}
          align={align}
          onClick={onClick}
          level={level}
          code={isCode}
          disabled={isDisable}
          mark={isMark}
          delete={isDelete}
          underline={isUnderline}
          isStrong={isStrong}
          isMedium={isMedium}
          ellipsis={isEllipsis}
          style={{
            color: colors[type] ? colors[type] : 'inherit',
            fontSize: size ? _.isNumber(size) ? `${isRem ? size * remPoint : size}px` : size : null,
            ...style
          }}
          {...safeObject({
            type: type === 'secondary' ? type : type !== 'text' ? type : null,
            id: htmlID || null
          })}
          {...applyHtmlData(dataSet)}
      >
        {children}
      </Root>
  )
}

Title.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  onClick: PropTypes.func,

  level: PropTypes.oneOf([1, 2, 3, 4]),
  type: PropTypes.string, // colors of theme or custom color
  isCode: PropTypes.bool,
  isDisable: PropTypes.bool,
  isMark: PropTypes.bool,
  isDelete: PropTypes.bool,
  isUnderline: PropTypes.bool,
  isStrong: PropTypes.bool,
  isMedium: PropTypes.bool,
  isEllipsis: PropTypes.bool,
  align: PropTypes.oneOf(['left', 'center', 'right']),
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isRem: PropTypes.bool
}

Title.defaultProps = {
  level: 1,
  type: '',
  isCode: false,
  isDisable: false,
  isMark: false,
  isDelete: false,
  isUnderline: false,
  isStrong: false,
  isMedium: false,
  isEllipsis: false,
  align: 'left',
  size: null,
  isRem: true
}

Title.displayName = 'Title'

export default withMargin({ displayName: 'Title' })(Title)
