import React from 'react'
import { storiesOf } from '@storybook/react'
import Text from './Text'
import Divider from '../Divider'

const stories = storiesOf('Typography/Text', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

const text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam autem cum delectus dolore dolorem dolores in incidunt itaque iure modi numquam perferendis, quae qui tenetur vitae voluptas! Nihil, voluptates?'

stories
  .add('default', () => (
    <Text>{text}</Text>
  ))
  .add('secondary', () => (
    <Text type="secondary">{text}</Text>
  ))
  .add('primary', () => (
      <Text type="primary">{text}</Text>
  ))
  .add('success', () => (
    <Text type="success">{text}</Text>
  ))
  .add('danger', () => (
    <Text type="danger">{text}</Text>
  ))
  .add('danger', () => (
    <Text type="danger">{text}</Text>
  ))
  .add('warning', () => (
    <Text type="warning">{text}</Text>
  ))
  .add('mark', () => (
    <Text isMark>{text}</Text>
  ))
  .add('deleted', () => (
    <Text isDelete>{text}</Text>
  ))
  .add('underline', () => (
    <Text isUnderline>{text}</Text>
  ))
  .add('strong and medium', () => (
    <>
      <Text isStrong>{text}</Text>
      <Divider/>
      <Text isMedium>{text}</Text>
    </>
  ))
