import _ from 'lodash'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Typography from 'antd/es/typography'
import withMargin from 'ui/common/withMargin'
import { colors } from 'styles'
import { Context } from 'ui/Provider/Context'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isStrong')
  _.unset(resolveProps, 'isMedium')
  _.unset(resolveProps, 'isRem')
  return <Typography.Paragraph {...resolveProps}/>
})`
  text-align: ${props => props.align};
  font-weight: ${props => (props.isStrong ? 600 : props.isMedium ? 500 : 'inherit')};
  transition: color .3s ease-in-out;
  cursor: ${props => (props.onClick ? 'pointer' : 'inherit')};

  & del {
    background: inherit;
  }
  
  & code {
    color: inherit;
  }
  
  & a, a:hover, a:visited, a:focus {
    color: inherit;
    text-decoration: inherit;
    cursor: pointer;
  }
`

const Paragraph = ({
  className, style, htmlID, dataSet,

  onClick, type, isCode, isDisable,
  isMark, isDelete, isUnderline, isStrong, isMedium, isEllipsis, align,
  size, isRem, isNativeMargin,
  children
}) => {
  const { remPoint } = useContext(Context)

  return (
      <Root
          className={className}
          align={align}
          onClick={onClick}
          code={isCode}
          disabled={isDisable}
          mark={isMark}
          delete={isDelete}
          underline={isUnderline}
          isStrong={isStrong}
          isMedium={isMedium}
          ellipsis={isEllipsis}
          style={{
            color: colors[type] ? colors[type] : 'inherit',
            fontSize: size ? _.isNumber(size) ? `${isRem ? size * remPoint : size}px` : size : null,
            ...style,
            marginBottom: style.marginBottom ? style.marginBottom : !isNativeMargin ? 0 : null
          }}
          {...safeObject({
            type: type === 'secondary' ? type : type !== 'text' ? type : null,
            id: htmlID || null
          })}
          {...applyHtmlData(dataSet)}
      >
        {children}
      </Root>
  )
}

Paragraph.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  onClick: PropTypes.func,
  type: PropTypes.string, // colors of theme or custom color
  isCode: PropTypes.bool,
  isDisable: PropTypes.bool,
  isMark: PropTypes.bool,
  isDelete: PropTypes.bool,
  isUnderline: PropTypes.bool,
  isStrong: PropTypes.bool,
  isMedium: PropTypes.bool,
  isEllipsis: PropTypes.bool,
  align: PropTypes.oneOf(['left', 'center', 'right']),
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isRem: PropTypes.bool,
  isNativeMargin: PropTypes.bool
}

Paragraph.defaultProps = {
  type: '',
  isCode: false,
  isDisable: false,
  isMark: false,
  isDelete: false,
  isUnderline: false,
  isStrong: false,
  isMedium: false,
  isEllipsis: false,
  align: 'left',
  size: null,
  isRem: true,
  isNativeMargin: true
}

Paragraph.displayName = 'Paragraph'

export default withMargin({ displayName: 'Paragraph' })(Paragraph)
