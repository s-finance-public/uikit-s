import React from 'react'
import PropTypes from 'prop-types'
import AntDivider from 'antd/es/divider'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Divider = ({
  className, style, htmlID, dataSet
}) => (
    <AntDivider
        className={className}
        style={style}
        {...safeObject({
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
    />
)

Divider.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object
}

Divider.displayName = 'Divider'

export default withMargin({ displayName: 'Divider' })(Divider)
