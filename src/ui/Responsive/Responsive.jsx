import React, { useContext } from 'react'
import { Context } from 'ui/Provider/Context'
import Mobile from './Mobile'
import Tablet from './Tablet'
import TabletAndPhone from './TabletAndPhone'
import BeforeDesktop from './BeforeDesktop'
import Desktop from './Desktop'
import MiddleDesktop from './MiddleDesktop'
import LargeDesktop from './LargeDesktop'

const Touch = ({ children }) => {
  const { responsiveModel } = useContext(Context)
  const { isTouch } = responsiveModel

  return isTouch ? children : null
}

export { Touch, Mobile, Tablet, TabletAndPhone, BeforeDesktop, Desktop, MiddleDesktop, LargeDesktop }
