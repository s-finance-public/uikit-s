import _ from 'lodash'
import React from 'react'
import Radio from 'antd/es/radio'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Group = ({ onChange, htmlID, dataSet, ...props }) => {
  const handleChange = e => {
    if (onChange) onChange(_.get(e, 'target.value', null))
  }
  return (
    <Radio.Group
        onChange={handleChange}
        {...props}
        {...applyHtmlData(dataSet)}
        {...safeObject({ id: htmlID || null })}
    />
  )
}

Group.displayName = 'RadioGroup'

export default Group
