import React from 'react'
import PropTypes from 'prop-types'
import AntdCol from 'antd/es/col'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Col = ({
  className, style, htmlID, dataSet,

  onClick,

  xs, sm, md, lg, span, offset, pull, push, children
}) => (
  <AntdCol
    xs={xs}
    sm={sm}
    md={md}
    lg={lg}
    span={span}
    offset={offset}
    pull={pull}
    push={push}
    className={className}
    style={style}
    onClick={onClick}
    {...safeObject({
      id: htmlID || null
    })}
    {...applyHtmlData(dataSet)}
  >
    {children}
  </AntdCol>
)

Col.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  onClick: PropTypes.func,

  xs: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  sm: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  md: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  lg: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  span: PropTypes.number,
  offset: PropTypes.number,
  pull: PropTypes.number,
  push: PropTypes.number
}

Col.defaultProps = {
  offset: 0,
  pull: 0,
  push: 0
}

Col.displayName = 'Col'

export default withMargin({ displayName: 'Col' })(Col)
