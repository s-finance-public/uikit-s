import React from 'react'
import notification from 'antd/es/notification'
import { colors } from 'styles'
import Icon from 'ui/Icon'

const MessageContainer = ({ message, color }) => (
  <div style={{ userSelect: 'none', color }}>
    {message}
  </div>
)

notification.config({
  closeIcon: <Icon icon="close" iconType="design" color="gray3" size={12} isRem={false}/>
})

export function notifySuccess(title = 'Успех', text, options = {}) {
  return notification.success({
    message: <MessageContainer message={title} color={colors.green9}/>,
    description: <MessageContainer message={text} color={colors.green7}/>,
    placement: 'bottomRight',
    style: {
      background: '#f6ffed',
      border: `solid 2px ${colors.green3}`
    },
    icon: <Icon iconType="design" icon="check-circle" color="green6" size={16} isRem={false}/>,
    ...options
  })
}

export function notifyInfo(title = 'Уведомление', text, options = {}) {
  return notification.info({
    message: <MessageContainer message={title} color={colors.blue8}/>,
    description: <MessageContainer message={text} color={colors.blue7}/>,
    placement: 'bottomRight',
    style: {
      background: colors.blue1,
      border: `solid 2px ${colors.blue3}`
    },
    icon: <Icon iconType="design" icon="info-circle" color="blue6" size={16} isRem={false}/>,
    ...options
  })
}

export function notifyError(title = 'Ошибка', text, options = {}) {
  return notification.error({
    message: <MessageContainer message={title} color={colors.red8}/>,
    description: <MessageContainer message={text} color={colors.red7}/>,
    placement: 'bottomRight',
    style: {
      background: '#fff1f0',
      border: `solid 2px ${colors.red3}`
    },
    icon: <Icon iconType="design" icon="warning" color="red5" size={16} isRem={false}/>,
    ...options
  })
}

export function notifyWarning(title = 'Предупреждение', text, options = {}) {
  return notification.warning({
    message: <MessageContainer message={title} color={colors.gold9}/>,
    description: <MessageContainer message={text} color={colors.gold7}/>,
    placement: 'bottomRight',
    style: {
      background: colors.gold1,
      border: `solid 2px ${colors.gold3}`
    },
    icon: <Icon iconType="design" icon="info-circle" color="gold6" size={16} isRem={false}/>,
    ...options
  })
}
