export const phoneNumberNormalizer = str => {
  const cleaned = `${str}`.replace(/\D/g, '')
  const match = cleaned.match(/^(\d|)?(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    // const intlCode = (match[1] ? '+1 ' : '')
    const intlCode = ''
    return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
  }

  return str
}
