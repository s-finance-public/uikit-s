import React, { Fragment } from 'react'
import Modal from 'antd/es/modal'

export default function ({
  title, content,
  okText = 'Да', okType = 'primary',
  cancelText = 'Отмена',
  isClosable = true,
  isCenter = false,
  width = 520,
  bodyStyle = {},
  icon = 'question-circle',
  onOk,
  onCancel,
  onAfterClose
}) {
  const Title = () => <Fragment>{title}</Fragment>
  const Content = () => <Fragment>{content}</Fragment>

  return Modal.confirm({
    title: <Title/>,
    content: <Content/>,
    okText,
    okType,
    cancelText,
    width,
    closable: isClosable,
    centered: isCenter,
    bodyStyle,
    icon,
    onOk,
    onCancel,
    afterClose: onAfterClose
  })
}
