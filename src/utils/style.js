import _ from 'lodash'

export const applyPolyfillCss = (field, value, isField = true) => {
  if (isField) {
    return `
  ${field}: ${value};
  ${field}: -webkit-${value};
  ${field}: -moz-${value};
  ${field}: -o-${value};
  `
  }

  return `
  ${field}: ${value};
  -webkit-${field}: ${value};
  -moz-${field}: ${value};
  -o-${field}: ${value};
  `
}

export const r2px = (value, point = 14) => (_.isNumber(value) ? `${value * point}px` : value)
