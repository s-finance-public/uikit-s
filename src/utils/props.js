import _ from 'lodash'

export const unsetFields = (fields = [], data) => {
  const resolveData = { ...data }
  _.each(fields, field => {
    _.unset(resolveData, field)
  })
  return resolveData
}
