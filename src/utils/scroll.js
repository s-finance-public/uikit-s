import { scroller, animateScroll, Element } from 'react-scroll'

const defaultOptions = {
  duration: 200,
  delay: 50,
  smooth: true
}

function scrollTo(name, options = {}) {
  scroller.scrollTo(name, { ...defaultOptions, ...options })
}

function scrollToTop(options = {}) {
  animateScroll.scrollToTop({ ...defaultOptions, ...options })
}

export { scrollTo, scrollToTop, Element }
