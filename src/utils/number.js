export const num2str = (n, labels) => {
  const number = Math.abs(n) % 100
  const n1 = number % 10
  if (number > 10 && number < 20) { return labels[2] }
  if (n1 > 1 && n1 < 5) { return labels[1] }
  if (n1 === 1) { return labels[0] }
  return labels[2]
}
