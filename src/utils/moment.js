import moment from 'moment'
import DATE_FORMATS from './dateFormats'
import 'moment/locale/ru'

moment.locale('ru')

function getAge({ date, format }) {
  const f = format || 'DD.MM.YYYY'
  return Number(moment(new Date(), f).diff(moment(date, f), 'years'))
}

function getDateByAge(age) {
  const now = new Date()
  now.setFullYear(now.getFullYear() - age)
  return now
}

function isValidDatePicker(date) {
  return !/_/ig.test(date) && moment(date, ['DD.MM.YYYY']).isValid()
}

const MONTHES = [
  {
    id: 0,
    name: 'Январь',
    short: 'Янв.'
  },
  {
    id: 1,
    name: 'Февраль',
    short: 'Февр.'
  },
  {
    id: 2,
    name: 'Март',
    short: 'Март'
  },
  {
    id: 3,
    name: 'Апрель',
    short: 'Апр.'
  },
  {
    id: 4,
    name: 'Май',
    short: 'Май'
  },
  {
    id: 5,
    name: 'Июнь',
    short: 'Июнь'
  },
  {
    id: 6,
    name: 'Июль',
    short: 'Июль'
  },
  {
    id: 7,
    name: 'Август',
    short: 'Авг.'
  },
  {
    id: 8,
    name: 'Сентябрь',
    short: 'Сент.'
  },
  {
    id: 9,
    name: 'Октябрь',
    short: 'Окт.'
  },
  {
    id: 10,
    name: 'Ноябрь',
    short: 'Нояб.'
  },
  {
    id: 11,
    name: 'Декабрь',
    short: 'Дек.'
  }
]

export { moment, getAge, getDateByAge, isValidDatePicker, MONTHES }
export default moment
