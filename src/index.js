import Popover from './ui/Popover'
import withFormProps from './ui/common/withFormProps'
import { applyHtmlData } from './utils/htmlData'
import withMargin from './ui/common/withMargin'
import Progress from './ui/Progress'
import Radio, { RadioGroup } from './ui/Radio'
import {
  Touch as ResponsiveTouch,
  Mobile as ResponsivePhone,
  Tablet as ResponsiveTablet,
  TabletAndPhone as ResponsiveTabletAndPhone,
  BeforeDesktop as ResponsiveBeforeDesktop,
  Desktop as ResponsiveDesktop,
  MiddleDesktop as ResponsiveMiddleDesktop,
  LargeDesktop as ResponsiveLargeDesktop
} from './ui/Responsive'
import Skeleton from './ui/Skeleton'
import Slider from './ui/Slider'
import Stepper from './ui/Stepper'
import Switch from './ui/Switch'
import Table from './ui/Table'
import LazyPagination from './ui/Table/LazyPagination'
import Tag from './ui/Tag'
import Alert from './ui/Alert'
import Badge from './ui/Badge'
import Block from './ui/Block'
import Button, { ButtonGroup } from './ui/Button'
import CardBlock from './ui/CardBlock'
import CheckBlock from './ui/CheckBlock'
import Checkbox, { CheckboxGroup } from './ui/Checkbox'
import Collapse, { Panel as CollapsePanel } from './ui/Collapse'
import Confirm from './utils/confirm'
import DatePicker from './ui/DatePicker'
import Description from './ui/Description'
import Drawer from './ui/Drawer'
import DropdownButton from './ui/DropdownButton'
import Dropzone from './ui/Dropzone'
import Row from './ui/Grid/Row'
import Col from './ui/Grid/Col'
import Icon from './ui/Icon'
import Image from './ui/Image'
import Input, { InputGroup } from './ui/Input'
import InputSelect from './ui/InputSelect'
import SelectPicker from './ui/SelectPicker'
import Label from './ui/Label'
import List from './ui/List'
import ListItem from './ui/List/ListItem'
import Loader from './ui/Loader'
import Modal from './ui/Modal'
import Menu from './ui/Menu'
import NavBar from './ui/NavBar'
import { notifySuccess, notifyError, notifyInfo, notifyWarning } from './utils/notification'
import Tooltip from './ui/Tooltip'
import { Text, Paragraph, Title, Divider } from './ui/Typography'
import Tabs from './ui/Tabs'
import Focusable from './ui/Focusable'
import Breadcrumb from './ui/Breadcrumb'
import MultiSelect from './ui/MultiSelect'
import UIKitProvider from './ui/Provider'
import { rusRegExp, phoneNumberNormalizer, moneyRubNormalizer, moneyRubDenormalizer, namesNormalizer, percentageFormatter } from './utils/formatters'
import { hexToRgba, lighten, addLight, darken, subtractLight } from './utils/colors'
import moment, { getAge, getDateByAge, isValidDatePicker } from './utils/moment'
import { num2str } from './utils/number'
import { unsetFields } from './utils/props'
import { scrollToTop, scrollTo, Element } from './utils/scroll'
import { applyPolyfillCss } from './utils/style'
import { colors, breakPoints, theme, viewPortsResolve } from './styles'
import { openFileLink } from './utils/document'
import { DOC_TYPES } from './utils/mime'
import { safeObject, isSafe } from './utils/object'
import { mountHook } from './hooks/mount'
import { Cyryllic, CyrillicName, CyrillicNameWithSpace, cyrillicPartFullName, CyrillicFullName } from './utils/validators/cyryllic'
import { validEmail } from './utils/validators/email'
import { validRequired } from './utils/validators/required'
import DATE_FORMATS, {
  DATE_FORMAT, FromNow, FormatDate, parseDate,
  dateLessThan, dateGreaterThan, lessThanCurrentDate, isEqualDates
} from './utils/dateFormats'
import { elementIdGenerator } from './utils/elementIdGenerator'
import { onlyNumberNormalizer } from './utils/formatters/number'
import { validUrl } from './utils/validators/url'
import NoData from './ui/NoData'
import Underline from './ui/Underline'

export {
  withFormProps,
  withMargin,
  applyHtmlData,
  Alert,
  Badge,
  Block,
  Button, ButtonGroup,
  CardBlock,
  CheckBlock,
  Checkbox, CheckboxGroup,
  Collapse, CollapsePanel,
  Confirm,
  DatePicker,
  Description,
  Drawer,
  DropdownButton,
  Dropzone,
  Row, Col,
  Icon,
  Image,
  Input,
  InputGroup,
  InputSelect,
  Label,
  List, ListItem,
  Loader,
  Modal,
  NavBar,
  notifySuccess, notifyError, notifyInfo, notifyWarning,
  Popover,
  Progress,
  Radio, RadioGroup,
  ResponsiveTouch, ResponsivePhone, ResponsiveTablet, ResponsiveTabletAndPhone, ResponsiveBeforeDesktop, ResponsiveDesktop, ResponsiveMiddleDesktop, ResponsiveLargeDesktop,
  Skeleton,
  SelectPicker,
  Slider,
  Stepper,
  Switch,
  Table,
  LazyPagination,
  Tag,
  Tooltip,
  Text, Title, Paragraph, Divider,
  Menu,
  MultiSelect,
  Tabs,
  Focusable,
  Breadcrumb,
  UIKitProvider,
  rusRegExp, phoneNumberNormalizer, moneyRubNormalizer, moneyRubDenormalizer, percentageFormatter, namesNormalizer,
  hexToRgba, lighten, addLight, darken, subtractLight,

  moment, getAge, getDateByAge, isValidDatePicker,
  DATE_FORMATS, DATE_FORMAT, FromNow, FormatDate, parseDate,
  dateLessThan, dateGreaterThan, lessThanCurrentDate, isEqualDates,

  num2str,
  unsetFields,
  scrollToTop, scrollTo, Element,
  colors, breakPoints, theme, viewPortsResolve,
  applyPolyfillCss,
  openFileLink,
  DOC_TYPES,
  safeObject, isSafe,
  mountHook,

  Cyryllic, CyrillicName, CyrillicNameWithSpace, cyrillicPartFullName, CyrillicFullName,
  validEmail,
  validRequired,

  elementIdGenerator,
  onlyNumberNormalizer,
  validUrl,
  NoData,
  Underline
}
