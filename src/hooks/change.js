import { useEffect, useState } from 'react'

export function useChange(field, defaultValue, { onChangeHook = null, state }) {
  const [value, setValue] = useState(defaultValue)
  const [isChange, setIsChangeValue] = useState(false)

  const onChange = (v, isChangeHook = true) => {
    setIsChangeValue(isChangeHook)
    setValue(v)
  }

  useEffect(() => {
    if (isChange && onChangeHook) onChangeHook({ field, value, state })
  }, [value])

  return [value, onChange]
}
