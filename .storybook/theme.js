import { create } from '@storybook/theming/create'
import logo from '../src/resources/sfinance_logo/logo-sfinance.svg'

export default create({
    base: 'light',

    colorPrimary: '#e7fbd5',
    colorSecondary: 'deepskyblue',

    // UI
    appBg: '#f4f7ff',
    appContentBg: '#f4f5f6',
    appBorderColor: '#c4ccd4',
    appBorderRadius: 4,

    // Typography
    fontBase: "'Plumb', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Hiragino Sans GB',\n" +
        "'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji',\n" +
        "'Segoe UI Emoji', 'Segoe UI Symbol'",
    fontCode: 'monospace',

    // Text colors
    textColor: '#171f33',
    textInverseColor: '#f4f5f6',

    barTextColor: '#7a818a',
    barSelectedColor: '#3eb5f1',
    barBg: '#fff',

    // Form colors
    inputBg: '#f4f5f6',
    inputBorder: '#c4ccd4',
    inputTextColor: '#171f33',
    inputBorderRadius: 4,

    brandTitle: 'S-Finance UI KIT',
    brandUrl: 'https://s-finance.pro/',
    brandImage: logo,
});
