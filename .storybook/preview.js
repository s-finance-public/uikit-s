import {addDecorator} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import React, {useEffect, useState} from "react"
import {createGlobalStyle} from 'styled-components'
import Provider from "ui/Provider"
import {Link} from "react-router-dom"
import MobileDetect from "mobile-detect"

const mobileDetect = new MobileDetect(window.navigator.userAgent)

const GlobalStyles = createGlobalStyle`
  body {
    padding: 2rem;
  }
`

const Root = ({children}) => (
    <div>
        <GlobalStyles/>
        {children}
    </div>
)

addDecorator(story => {
    const [width, setWidth] = useState(window.innerWidth)

    useEffect(() => {
        window.addEventListener('resize', () => {
            setWidth(window.innerWidth)
        })
    })

    return (
        <Root>
            <Provider
                width={width}
                Link={({to, children}) => <Link to={to}>{children}</Link>}
                responsiveModel={{
                    isTouch: !!(mobileDetect.mobile() || mobileDetect.phone() || mobileDetect.tablet())
                }}
                label={{
                    weight: 400
                }}
                remPoint={14}
            >
                {story()}
            </Provider>
        </Root>
    )
})


addDecorator(withInfo)
