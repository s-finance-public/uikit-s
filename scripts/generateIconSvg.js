import _ from 'lodash'
import fp from 'lodash/fp'
import svgr from '@svgr/core'
import path from 'path'
import fs from 'fs'
import os from 'os'
import { v4 as uuid } from 'uuid'

const ICON_PREFIX = process.env.PREFIX
if (!ICON_PREFIX) {
  console.error('Set env `PREFIX`')
  os.exit(1)
}
const ICONS_PATH = path.resolve('src/resources/icons', ICON_PREFIX)
const OUTPUT_ICONS_PATH = path.resolve(`src/ui/Icon/${ICON_PREFIX}-icons`)

if (!fs.existsSync(OUTPUT_ICONS_PATH)) {
  fs.mkdirSync(OUTPUT_ICONS_PATH)
}

fs.readdir(ICONS_PATH, (err, items) => {
  if (err) {
    console.error('Cannot read icons')
    return
  }

  _.each(items, async item => {
    const svg = fs.readFileSync(path.resolve(ICONS_PATH, item), 'utf8')

    const jsCode = await svgr(svg, { icon: true }, {
      componentName: 'Icon'
    })

    fs.writeFileSync(path.resolve(`${OUTPUT_ICONS_PATH}/${item.replace('.svg', '.jsx')}`),
      jsCode)
  })

  const icons = fp.map(item => ({
    name: item.replace('.svg', ''),
    id: `U${uuid().replace(/-*/ig, '')}`
  }))(items)

  fs.writeFileSync(path.resolve(`${OUTPUT_ICONS_PATH}/index.js`), `
${fp.map(icon => `import ${icon.id} from './${icon.name}'`)(icons).join('\n')}

export default {
  ${fp.map(icon => `'${icon.name}': ${icon.id},`)(icons).join('\n')}
}
`.trim())

  console.log(`Finish generate ${ICON_PREFIX} icons`)
})
