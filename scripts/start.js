const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server')
const webpackConfig = require('../config/webpack/webpack.config')
const config = require('../config/config')

new WebpackDevServer(webpack(webpackConfig), {
  publicPath: '/',
  hot: true,
  historyApiFallback: true,
  quiet: false,
  noInfo: false,
  stats: {
    assets: true,
    colors: true,
    version: true,
    hash: true,
    timings: true,
    chunks: false,
    chunkModules: false
  }
}).listen(config.server.port, '0.0.0.0', err => {
  if (err) {
    console.error(err)
  }
})
