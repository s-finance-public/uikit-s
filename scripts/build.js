const webpack = require('webpack')
const webpackConfig = require('../config/webpack/webpack.config')

webpack(webpackConfig).run((err, stats) => {
  if (err) {
    console.error(err)
  }
  console.log(stats.toString({
    assets: true,
    colors: true,
    version: true,
    hash: true,
    timings: true,
    chunks: false,
    chunkModules: false
  }))
})
